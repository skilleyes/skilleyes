//
//  CellSkill.swift
//  Skilleyes
//
//  Created by Mac HD on 28/11/18.
//  Copyright © 2018 Mac HD. All rights reserved.
//

import UIKit

class CellSkill: UITableViewCell {

    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblPrice: UILabel!
    @IBOutlet var txtVewDesc: UITextView!
    @IBOutlet var btnEdit: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
