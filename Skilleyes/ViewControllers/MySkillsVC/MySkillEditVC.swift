//
//  MySkillEditVC.swift
//  Skilleyes
//
//  Created by Mac HD on 28/11/18.
//  Copyright © 2018 Mac HD. All rights reserved.
//

import UIKit

class MySkillEditVC: UIViewController, UITextFieldDelegate, UITextViewDelegate {

    //Outlets
    @IBOutlet var lblMySkil: UILabel!
    @IBOutlet var lblPrice: UILabel!
    @IBOutlet var lblDetail: UILabel!
    @IBOutlet var lblAmt: UILabel!
    @IBOutlet var txtFldSkil: UITextField!
    @IBOutlet var txtVewDetail: UITextView!
    @IBOutlet var vewMain: UIView!
    @IBOutlet var btnSave: UIButton!
    
    //Variables
    var counterValue = 1
    let STATUSBAR_COLOR = #colorLiteral(red: 0.8941176471, green: 0.8980392157, blue: 0.9019607843, alpha: 1)
    
    override func viewDidLoad() {
        super.viewDidLoad()

        lblAmt.text = "\(counterValue)"
        
        txtFldSkil.layer.cornerRadius = 15
        txtFldSkil.clipsToBounds = true
        
        txtVewDetail.layer.cornerRadius = 15
        txtVewDetail.clipsToBounds = true
        
//        vewMain.layer.cornerRadius = 15
//        vewMain.clipsToBounds = true
        
        shadowView(view: vewMain)
        
        changeLanguage()
        
        //setup status bar
        self.setNeedsStatusBarAppearanceUpdate()
        setStatusBarBackgroundColor(color: STATUSBAR_COLOR)
    }
    
    //MARK: Status bar text color
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    func setStatusBarBackgroundColor(color:UIColor)  {
        let statusBar = (UIApplication.shared.value(forKey: "statusBarWindow") as AnyObject).value(forKey: "statusBar") as? UIView
        statusBar?.backgroundColor = color
    }
    
    func shadowView(view:UIView)
    {
        // corner radius
        view.layer.cornerRadius = 15
        
        // shadow
        view.layer.shadowColor = UIColor.darkGray.cgColor
        view.layer.shadowOffset = CGSize(width: 3, height: 3)
        view.layer.shadowOpacity = 0.7
        view.layer.shadowRadius = 2.0
    }
    
    //MARK: app language method
    func changeLanguage()
    {
        if (UserDefaults.standard.string(forKey: "Germany") != nil)
        {
            lblMySkil.text = "meine Fähigkeiten"
            lblPrice.text = "Preis / min"
            lblDetail.text = "Einzelheiten"
            
            btnSave.setTitle("Sparen", for: .normal)
            
        }else if (UserDefaults.standard.string(forKey: "America") != nil)
        {
            lblMySkil.text = "mySkills"
            lblPrice.text = "Price / min"
            lblDetail.text = "Details"
            
            btnSave.setTitle("Save", for: .normal)
        }
    }
    
    //MARK: action method
    @IBAction func actionSave(_ sender: Any)
    {
        
    }
    
    @IBAction func actionDelete(_ sender: Any)
    {
        
    }
    
    @IBAction func actionBack(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionPlus(_ sender: Any)
    {
        lblAmt.text = "\(counterValue)"
        counterValue += 1;
        lblAmt.text = "\(counterValue)"
    }
    
    @IBAction func actionMinus(_ sender: Any)
    {
        if(counterValue != 1){
            counterValue -= 1;
        }
        lblAmt.text = "\(counterValue)"
    }


}
