//
//  WalletVC.swift
//  Skilleyes
//
//  Created by Mac HD on 28/11/18.
//  Copyright © 2018 Mac HD. All rights reserved.
//

import UIKit

class WalletVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    //Outlets
    @IBOutlet var tblVewList: UITableView!
    @IBOutlet var btn1: UIButton!
    @IBOutlet var btn2: UIButton!
    @IBOutlet var lblHead: UILabel!
    @IBOutlet var lblHstry: UILabel!
    @IBOutlet var lblDay: UILabel!
    @IBOutlet var lblmySkil: UILabel!
    @IBOutlet var lblTotal: UILabel!
    
    //Variable
    let STATUSBAR_COLOR = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    
    override func viewDidLoad() {
        super.viewDidLoad()

        changeLanguage()
        
        //setup status bar
        self.setNeedsStatusBarAppearanceUpdate()
        setStatusBarBackgroundColor(color: STATUSBAR_COLOR)
        
        btn2.layer.cornerRadius = 20
        btn2.clipsToBounds = true
//        btn1.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        btn1.layer.cornerRadius = 20
        btn1.clipsToBounds = true
    }

    //MARK: Status bar text color
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    func setStatusBarBackgroundColor(color:UIColor)  {
        let statusBar = (UIApplication.shared.value(forKey: "statusBarWindow") as AnyObject).value(forKey: "statusBar") as? UIView
        statusBar?.backgroundColor = color
    }
    
    //MARK: app language method
    func changeLanguage()
    {
        if (UserDefaults.standard.string(forKey: "Germany") != nil)
        {
            lblHead.text = "Brieftasche"
            lblHstry.text = "Geschichte"
            lblDay.text = "Datum"
            lblmySkil.text = "mySkill"
            lblTotal.text = "Gesamt"
            
            btn1.setTitle("Verdienste", for: .normal)
            btn2.setTitle("Ausgaben", for: .normal)
            
        }else if (UserDefaults.standard.string(forKey: "America") != nil)
        {
            lblHead.text = "Wallet"
            lblHstry.text = "History"
            lblDay.text = "Date"
            lblmySkil.text = "mySkill"
            lblTotal.text = "Total"
            
            btn1.setTitle("Earnings", for: .normal)
            btn2.setTitle("Spendings", for: .normal)
        }
    }
    
    //MARK: action method
    @IBAction func actionEarn(_ sender: Any)
    {
        btn2.layer.cornerRadius = 20
        btn2.clipsToBounds = true
        
        btn2.backgroundColor = #colorLiteral(red: 0.9294117647, green: 0.9333333333, blue: 0.937254902, alpha: 1)
        btn2.setTitleColor(UIColor.darkGray, for: .normal)
        btn2.setBackgroundImage(UIImage(named: ""), for: .normal)
        
        btn1.setBackgroundImage(UIImage(named: "login-bg"), for: .normal)
        btn1.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        btn1.setTitleColor(UIColor.white, for: .normal)
    }
    
    @IBAction func actionSpend(_ sender: Any)
    {
        btn1.layer.cornerRadius = 20
        btn1.clipsToBounds = true
        
        btn1.backgroundColor = #colorLiteral(red: 0.9294117647, green: 0.9333333333, blue: 0.937254902, alpha: 1)
        btn1.setTitleColor(UIColor.darkGray, for: .normal)
        btn1.setBackgroundImage(UIImage(named: ""), for: .normal)
        
        btn2.setBackgroundImage(UIImage(named: "login-bg"), for: .normal)
        btn2.setTitleColor(UIColor.white, for: .normal)
        btn2.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        
    }
    
    @IBAction func actionBack(_ sender: Any)
    {
        self.view.endEditing(true)
        self.frostedViewController.view.endEditing(true)
        self.frostedViewController.presentMenuViewController()
    }
    
    //MARK: tableview delegate methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        var cell = tableView.dequeueReusableCell(withIdentifier: "CellWallet") as? CellWallet
        if cell == nil {
            let tempArray = Bundle.main.loadNibNamed("CellWallet", owner: self, options: nil)
            cell = tempArray?.first  as? CellWallet
        }
        
//        cell?.btnEdit.addTarget(self, action: #selector(actionEdit(sender:)), for: .touchUpInside)
//        cell?.btnEdit.tag = indexPath.row
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 50
    }
    
    

}
