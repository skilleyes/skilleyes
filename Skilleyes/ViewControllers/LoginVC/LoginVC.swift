//
//  LoginVC.swift
//  Skilleyes
//
//  Created by Mac HD on 22/11/18.
//  Copyright © 2018 Mac HD. All rights reserved.
//

import UIKit

class LoginVC: UIViewController, UITextFieldDelegate{

    //Outlets
    @IBOutlet weak var txtFldEmail: UITextField!
    @IBOutlet weak var txtFldPaswrd: UITextField!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var btnRegstr: UIButton!
    
    //Varioables
    let STATUSBAR_COLOR = #colorLiteral(red: 0.05882352941, green: 0.2274509804, blue: 0.3607843137, alpha: 1)
    var hud: MBProgressHUD = MBProgressHUD()
    var strEmail : String!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //-- For Testing ---
        txtFldEmail.text = "amit@g.com"
        txtFldPaswrd.text = "qwerty"
        //-------
        
        //set textfields
        self.paddingTextField(textField: txtFldEmail)
        self.paddingTextField(textField: txtFldPaswrd)
        
        //setup status bar
        self.setNeedsStatusBarAppearanceUpdate()
        setStatusBarBackgroundColor(color: STATUSBAR_COLOR)
    }
    
    //MARK: Status bar text color
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func setStatusBarBackgroundColor(color:UIColor)  {
        let statusBar = (UIApplication.shared.value(forKey: "statusBarWindow") as AnyObject).value(forKey: "statusBar") as? UIView
        statusBar?.backgroundColor = color
    }
    
    //MARK: TextField method
    func paddingTextField(textField:UITextField)
    {
        //padding
        let padding = UIView.init(frame: CGRect.init(x: 0, y: 0, width: 10, height: 20))
        padding.backgroundColor = UIColor.clear
        textField.leftView = padding
        textField.leftViewMode = UITextField.ViewMode.always
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        txtFldEmail.resignFirstResponder()
        txtFldPaswrd.resignFirstResponder()
        return true
    }

    //MARK: action methods
    @IBAction func actionLogin(_ sender: Any)
    {
//        let appDel = UIApplication.shared.delegate as! AppDelegate
//        appDel.pubNubPublish()
        
//        //Navigate to Incoming Call View Controller after receive a Message
//        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//        //let vc = storyBoard.instantiateViewController(withIdentifier: "IncomingCallVC") as! IncomingCallVC
//        let vc = storyBoard.instantiateViewController(withIdentifier: "IncomingCallVCWithCamera") as! IncomingCallVCWithCamera
//        self.navigationController?.pushViewController(vc, animated: true)
        
        if (txtFldEmail.text == "") {
            AppDelegate.showAlert(message: "Please enter email", title: "Message")
        }else if (txtFldPaswrd.text == "") {
            AppDelegate.showAlert(message: "Please enter minimum 6 characters password", title: "Message")
        }else
        {
            if CheckInternet.Connection(){
                login()
            }else{
                AppDelegate.showAlert(message: "Your Device is not connected with internet", title: "")
            }
        }
    }
    
    @IBAction func actionGermany(_ sender: Any)
    {
        let Lngstatus = "Germany"
        UserDefaults.standard.setValue(Lngstatus, forKey: "Germany")
            
        UserDefaults.standard.removeObject(forKey: "America")
        
        btnLogin.setTitle("Anmeldung", for: .normal)
        btnRegstr.setTitle("Noch keinen Account? Hier registrieren", for: .normal)
        
        txtFldEmail.attributedPlaceholder = NSAttributedString(string: "Email",attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        txtFldPaswrd.attributedPlaceholder = NSAttributedString(string: "Passwort",attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
    }

    @IBAction func actionAmerica(_ sender: Any)
    {
        let Lngstatus = "America"
        UserDefaults.standard.setValue(Lngstatus, forKey: "America")
        
        UserDefaults.standard.removeObject(forKey: "Germany")
        
        btnLogin.setTitle("Login", for: .normal)
        btnRegstr.setTitle("No account yet? Register here", for: .normal)
        
        txtFldEmail.attributedPlaceholder = NSAttributedString(string: "Email",attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        txtFldPaswrd.attributedPlaceholder = NSAttributedString(string: "Password",attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
    }
    
    @IBAction func actionRegister(_ sender: Any)
    {
        let objLoginVC:RegisterVC = storyboard?.instantiateViewController(withIdentifier: "RegisterVC") as! RegisterVC
        self.navigationController?.pushViewController(objLoginVC, animated: false)
    }
    
    //MARK: Service calling
    func login()
    {
        hud = MBProgressHUD.showAdded(to: self.view, animated: true)
        hud.mode = MBProgressHUDMode.indeterminate
        hud.label.text = "Loading..."
        
        self.strEmail = self.txtFldEmail.text
        print(self.strEmail)
        
        var dict = Dictionary<String, String>()
        let fcmToken = UserDefaults.standard.string(forKey: "FCMTOKEN")
        
        dict = ["username": "\(txtFldEmail.text!)","password": "\(txtFldPaswrd.text!)","registration_token": "\(fcmToken!)"]
        
        print(dict)
        _ = WebserviceCall_Swift.shared.initWithDelegate(delegate: self, callBack: #selector(self.ServiceHandler(sender:)))
        DispatchQueue.global(qos: .background).async{
            WebserviceCall_Swift.shared.serviceCall(parameterDict: dict , url : "login")
        }
    }
    
    func online(email:String)
    {
        var dict = Dictionary<String, String>()
        
        dict = ["email": email]
        
        print(dict)
        _ = WebserviceCall_Swift.shared.initWithDelegate(delegate: self, callBack: #selector(self.ServiceHandler(sender:)))
        DispatchQueue.global(qos: .background).async{
            WebserviceCall_Swift.shared.serviceCall(parameterDict: dict , url : "online")
        }
    }
    
    func deviceDetail(email: String)
    {
        hud = MBProgressHUD.showAdded(to: self.view, animated: true)
        hud.mode = MBProgressHUDMode.indeterminate
        hud.label.text = "Loading..."
        
        var dict = Dictionary<String, String>()
        
        var deviceType = UIDevice.current.modelName
        print("device name--",deviceType)
        
        let bounds = UIScreen.main.bounds
        let width = bounds.size.width
        let height = bounds.size.height
        
        print("device screen width--",width)
        print("device screen height--",height)
        
        let systemVersion = UIDevice.current.systemVersion
        print("os version--",systemVersion)
        
        if (deviceType == ""){
            deviceType = "simulator"
        }
        dict = ["email": self.strEmail, "height": "\(height)", "width": "\(width)", "os_version": "\(systemVersion)", "api_level": "22", "device": "iOS", "model": deviceType, "product": "iPone"]
        
        print(dict)
        _ = WebserviceCall_Swift.shared.initWithDelegate(delegate: self, callBack: #selector(self.ServiceHandler(sender:)))
        DispatchQueue.global(qos: .background).async{
            WebserviceCall_Swift.shared.serviceCall(parameterDict: dict , url : "device")
        }
    }
    
    
    //MARK: Service handler
    @objc func ServiceHandler(sender:AnyObject)
    {
        DispatchQueue.main.async
            {
                MBProgressHUD.hide(for: self.view, animated: true)
                if sender.isKind(of: NSString.self)
                {
                    Singleton.shared.error = sender as! String
                    if Singleton.shared.error == "No Internet Connection"
                    {
                        AppDelegate.showAlert(message: "No Internet Connection", title: "Connection Failed!")
                    }
                    else
                    {
                        return
                    }
                }
                
                if sender.isKind(of: NSArray.self)
                {
                    let arrResult = sender as! NSArray
                    let json = arrResult.object(at: 0) as! NSDictionary
                    let serviceStr = arrResult.object(at: 1) as! String
                    
                    if serviceStr == "login"
                    {
                        if json.object(forKey: "access_token") as? String != nil
                        {
                            let delItemInfo = json.object(forKey: "access_token") as! String
                            print(delItemInfo)
                            
                            UserDefaults.standard.setValue(delItemInfo, forKey: "UserInfo")
                            UserDefaults.standard.setValue(delItemInfo, forKey: "login_accesstoken")
                            
                            self.txtFldEmail.text = ""
                            self.txtFldPaswrd.text = ""
                            
                            print(self.strEmail)
                            if self.strEmail.length > 0{
                                UserDefaults.standard.setValue(self.strEmail, forKey: "email")
                            }
                            
                            self.online(email: self.strEmail)
                            
                        }else
                        {
                            let err = json.object(forKey: "message") as! NSDictionary
                            print(err)
                            if err.object(forKey: "email") as? NSArray != nil
                            {
                                let msginfo = err.object(forKey: "email") as! NSArray
                                print(msginfo)
                                let msg = msginfo.object(at: 0) as! String
                                print(msg)
                                AppDelegate.showAlert(message: msg, title: "Info")
                            }else {
                                let err = json.object(forKey: "message") as! String
                                AppDelegate.showAlert(message: err, title: "Info")
                            }
                        }
                    }
                    else if serviceStr == "online"
                    {
                        if json.object(forKey: "email") as? String != nil
                        {
                            let delItemInfo = json.object(forKey: "online_status") as! Bool
                            print(delItemInfo)

                            print(self.strEmail)
                            
                             let clientId = String.init(format:"%@", json.object(forKey: "id") as! CVarArg) //json.object(forKey: "id") as! String
                                if clientId.length > 0{
                                    UserDefaults.standard.set(clientId, forKey: "client_id")
                                }
                            
                            let rootNavVC:RootVC = self.storyboard?.instantiateViewController(withIdentifier: "RootVC") as! RootVC
                            //let contentNavController = self.storyboard?.instantiateViewController(withIdentifier: "contentController") as! NavigationVC
                            self.navigationController?.pushViewController(rootNavVC, animated: false)
                            
//                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
//                            appDelegate.window?.rootViewController = contentNavController //rootNavVC
                            
                        }else
                        {
                           let err = json.object(forKey: "message") as! String
                           AppDelegate.showAlert(message: err, title: "Info")
                         
                        }
                    }
                    else if serviceStr == "device"
                    {
                        if json.object(forKey: "os_version") as? String != nil
                        {
                            let delItemInfo = String.init(format:"%@", json.object(forKey: "id") as! CVarArg)
                            print(delItemInfo)
                            
                            UserDefaults.standard.setValue(delItemInfo, forKey: "deviceId")
                            
                        }else
                        {
                            let err = json.object(forKey: "message") as! String
                            AppDelegate.showAlert(message: err, title: "Info")
                        }
                    }
                }
        }
    }

}
