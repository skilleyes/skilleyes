//
//  RegisterVC.swift
//  Skilleyes
//
//  Created by Mac HD on 22/11/18.
//  Copyright © 2018 Mac HD. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

class RegisterVC: UIViewController, UITextFieldDelegate {

    //Outlets
    @IBOutlet weak var txtFldName: UITextField!
    @IBOutlet weak var txtFldEmail: UITextField!
    @IBOutlet weak var txtFldPhNo: UITextField!
    @IBOutlet weak var txtFldPaswrd: UITextField!
    @IBOutlet weak var btnCreateAcount: UIButton!
    
    //Varioables
    let STATUSBAR_COLOR = #colorLiteral(red: 0.05882352941, green: 0.2274509804, blue: 0.3607843137, alpha: 1)
    var hud: MBProgressHUD = MBProgressHUD()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        changeLanguage()
        
        //set textfields
        self.paddingTextField(textField: txtFldName)
        self.paddingTextField(textField: txtFldEmail)
        self.paddingTextField(textField: txtFldPhNo)
        self.paddingTextField(textField: txtFldPaswrd)
        
        //setup status bar
        self.setNeedsStatusBarAppearanceUpdate()
        setStatusBarBackgroundColor(color: STATUSBAR_COLOR)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        changeLanguage()
    }
    
    //MARK: Status bar text color
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func setStatusBarBackgroundColor(color:UIColor)  {
        let statusBar = (UIApplication.shared.value(forKey: "statusBarWindow") as AnyObject).value(forKey: "statusBar") as? UIView
        statusBar?.backgroundColor = color
    }
    
    //MARK: TextField method
    func paddingTextField(textField:UITextField)
    {
        //padding
        let padding = UIView.init(frame: CGRect.init(x: 0, y: 0, width: 10, height: 20))
        padding.backgroundColor = UIColor.clear
        textField.leftView = padding
        textField.leftViewMode = UITextField.ViewMode.always
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
//        txtFldPaswrd.resignFirstResponder()
        return true
    }
    
    //MARK: app language method
    func changeLanguage()
    {
        if (UserDefaults.standard.string(forKey: "Germany") != nil)
        {
            txtFldName.attributedPlaceholder = NSAttributedString(string: "Name",attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
            txtFldEmail.attributedPlaceholder = NSAttributedString(string: "Email",attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
            txtFldPhNo.attributedPlaceholder = NSAttributedString(string: "telefonnummer",attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
            txtFldPaswrd.attributedPlaceholder = NSAttributedString(string: "Passwort",attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
            
            btnCreateAcount.setTitle("Benutzerkonto anlegen", for: .normal)
            
        }else if (UserDefaults.standard.string(forKey: "America") != nil)
        {
            txtFldName.attributedPlaceholder = NSAttributedString(string: "Name",attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
            txtFldEmail.attributedPlaceholder = NSAttributedString(string: "Email",attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
            txtFldPhNo.attributedPlaceholder = NSAttributedString(string: "Phone Number",attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
            txtFldPaswrd.attributedPlaceholder = NSAttributedString(string: "Password",attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
            
            btnCreateAcount.setTitle("Create Account", for: .normal)
        }
    }
    
    //MARK: action method
    @IBAction func actionRegister(_ sender: Any)
    {
        if (txtFldName.text == "") {
            AppDelegate.showAlert(message: "Please enter name", title: "Message")
        }else if (txtFldEmail.text == "") {
            AppDelegate.showAlert(message: "Please enter email", title: "Message")
        }else if (txtFldPaswrd.text == "") {
            AppDelegate.showAlert(message: "Please enter minimum 6 characters password", title: "Message")
        }else if (txtFldPhNo.text == "") {
            AppDelegate.showAlert(message: "Please enter phone number", title: "Message")
        }else
        {
           if CheckInternet.Connection(){
              signUp()
        }else{
             AppDelegate.showAlert(message: "Your Device is not connected with internet", title: "")
          }
       }
    }
    
    //MARK: Service calling
    func signUp()
    {
        hud = MBProgressHUD.showAdded(to: self.view, animated: true)
        hud.mode = MBProgressHUDMode.indeterminate
        hud.label.text = "Loading..."
        
        var dict = Dictionary<String, String>()
        
        let fcmToken = UserDefaults.standard.string(forKey: "FCMTOKEN")
        
        dict = ["name": "\(txtFldName.text!)","email": "\(txtFldEmail.text!)","phone_number": "\(txtFldPhNo.text!)","password": "\(txtFldPaswrd.text!)","registration_token": "\(fcmToken!)"]
        
        print(dict)
        _ = WebserviceCall_Swift.shared.initWithDelegate(delegate: self, callBack: #selector(self.ServiceHandler(sender:)))
        DispatchQueue.global(qos: .background).async{
            WebserviceCall_Swift.shared.serviceCall(parameterDict: dict , url : "register")
        }
    }
    
    //MARK: Service handler
    @objc func ServiceHandler(sender:AnyObject)
    {
        DispatchQueue.main.async
            {
                MBProgressHUD.hide(for: self.view, animated: true)
                if sender.isKind(of: NSString.self)
                {
                    Singleton.shared.error = sender as! String
                    if Singleton.shared.error == "No Internet Connection"
                    {
                        AppDelegate.showAlert(message: "No Internet Connection", title: "Connection Failed!")
                    }
                    else
                    {
                        return
                    }
                }
                
                if sender.isKind(of: NSArray.self)
                {
                    let arrResult = sender as! NSArray
                    let json = arrResult.object(at: 0) as! NSDictionary
                    let serviceStr = arrResult.object(at: 1) as! String
                    
                    if serviceStr == "register"
                    {
                        if json.object(forKey: "access_token") as? String != nil
                        {
                            let delItemInfo = json.object(forKey: "access_token") as! String
                            print(delItemInfo)
                            
                            UserDefaults.standard.setValue(delItemInfo, forKey: "UserInfo")
                            
                            if self.txtFldName.text!.length > 0{
                                UserDefaults.standard.set(self.txtFldName.text, forKey: "name")
                            }
                            
                            if self.txtFldEmail.text!.length > 0{
                                UserDefaults.standard.set(self.txtFldEmail.text, forKey: "email")
                            }
                            
                            if self.txtFldPhNo.text!.length > 0{
                                UserDefaults.standard.set(self.txtFldPhNo.text, forKey: "mobile_no")
                            }
                            
                            self.txtFldName.text = ""
                            self.txtFldEmail.text = ""
                            self.txtFldPaswrd.text = ""
                            self.txtFldPhNo.text = ""
                            
                            AppDelegate.showAlert(message: "Registeration have been successfully. Please login using same credentials", title: "Info")
                            
                            //let objLoginVC:RootVC = self.storyboard?.instantiateViewController(withIdentifier: "RootVC") as! RootVC
                            //self.navigationController?.pushViewController(objLoginVC, animated: false)
                            
                            let objLoginVC:LoginVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                            self.navigationController?.pushViewController(objLoginVC, animated: false)

                        }else
                        {
                            let err = json.object(forKey: "errors") as! NSDictionary
                            print(err)
                            if err.object(forKey: "email") as? NSArray != nil
                            {
                                let msginfo = err.object(forKey: "email") as! NSArray
                                print(msginfo)
                                let msg = msginfo.object(at: 0) as! String
                                print(msg)
                                AppDelegate.showAlert(message: msg, title: "Info")
                            }else {
                                let err = json.object(forKey: "message") as! String
                                AppDelegate.showAlert(message: err, title: "Info")
                            }
                        }
                    }
                }
        }
    }

}
