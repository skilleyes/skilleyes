//
//  AppDelegate.swift
//  Skilleyes
//
//  Created by Mac HD on 22/11/18.
//  Copyright © 2018 Mac HD. All rights reserved.
//

import UIKit
import PubNub
import IQKeyboardManagerSwift
import Firebase
import UserNotifications
import CallKit
import PushKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, PNObjectEventListener, CXProviderDelegate, PKPushRegistryDelegate {

    var window: UIWindow?
    static let shared =  UIApplication.shared.delegate as! AppDelegate
    
    let gcmMessageIDKey = "gcm.message_id"
    
    //MARK: Properties
    var client : PubNub!
    var pubNubMessage : PNMessageResult!
    
    var channel1: NSString?
    var channel2: NSString?
    var channelGroup1: NSString?
    var subKey: NSString?
    var pubKey: NSString?
    var authKey: NSString!
    var application: UIApplication!
    
    var timer: Timer?
    var myConfig : PNConfiguration?
    
    class func sharedInstance() -> AppDelegate
    {
        return UIApplication.shared.delegate as! AppDelegate
    }

    class func getPubNubMessage() -> PNMessageResult
    {
        return shared.pubNubMessage
    }
    
    class func getPubNubClient() -> PubNub
    {
        return shared.client
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
       
        self.application = application
        
        //MARK: Enable IQKeyboardManager
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = true
    
        //MARK:  Configurations for Firebase
        FirebaseApp.configure()
        application.registerForRemoteNotifications()
        requestNotificationAuthorization(application: application)
        if let userInfo = launchOptions?[UIApplication.LaunchOptionsKey.remoteNotification] {
            NSLog("[RemoteNotification] applicationState: \(applicationStateString) didFinishLaunchingWithOptions for iOS9: \(userInfo)")
        }
    
        return true
    }
    
    //MARK: Call Ringing Handler's
    func startRingin() -> Void {
        
                let provider = CXProvider(configuration: CXProviderConfiguration(localizedName: "Skilleyes"))
                provider.setDelegate(self, queue: nil)
                let update = CXCallUpdate()
                update.remoteHandle = CXHandle(type: .generic, value: "Skilleyes")
                update.hasVideo = true
                provider.reportNewIncomingCall(with: UUID(), update: update, completion: { error in })
        
//        let registry = PKPushRegistry(queue: nil)
//        registry.delegate = self
//        registry.desiredPushTypes = [PKPushType.voIP]
    }
    
    func pushRegistry(_ registry: PKPushRegistry, didReceiveIncomingPushWith payload: PKPushPayload, for type: PKPushType, completion: @escaping () -> Void) {
        let config = CXProviderConfiguration(localizedName: "Skilleyes")
        config.iconTemplateImageData = UIImage(named: "skilleye")!.pngData()
        config.ringtoneSound = "ringtone.caf"
        if #available(iOS 11.0, *) {
            config.includesCallsInRecents = false
        } else {
            // Fallback on earlier versions
        }
        config.supportsVideo = true
        let provider = CXProvider(configuration: config)
        provider.setDelegate(self, queue: nil)
        let update = CXCallUpdate()
        update.remoteHandle = CXHandle(type: .generic, value: "Skilleyes")
        update.hasVideo = true
        provider.reportNewIncomingCall(with: UUID(), update: update, completion: { error in })
    }
    
    func providerDidReset(_ provider: CXProvider) {
    }
    
    func provider(_ provider: CXProvider, perform action: CXAnswerCallAction) {
        action.fulfill()
        
        //        //Open Popup for Camera Choose
        //        let cameraChooseView = cameraChoosePopup.init(frame: self.view.frame)
        //        cameraChooseView.delegate = self
        //        self.view.addSubview(cameraChooseView)
        
                //Navigate to Incoming Call View Controller after receive a Message
                let navigationController = self.application.windows[0].rootViewController as! UINavigationController
                let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyBoard.instantiateViewController(withIdentifier: "IncomingCallVCWithCamera") as! IncomingCallVCWithCamera
                //let vc = storyBoard.instantiateViewController(withIdentifier: "InquiryVC") as! InquiryVC
        
                //navigationController.pushViewController(vc, animated: true)
        
        //vc.performSegue(withIdentifier: "videostreamSegue", sender: nil)
        //videostreamSegue
        //let contentNavController = storyBoard.instantiateViewController(withIdentifier: "contentController") as! NavigationVC
        let sideMenuNavigationController :RootVC = storyBoard.instantiateViewController(withIdentifier: "RootVC") as! RootVC
        //contentNavController.pushViewController(vc, animated: true)
        
        //let rootNaVController = UIApplication.shared.keyWindow!.rootViewController as! RootVC
        //rootNaVController.
        
//        let contentNavController = storyBoard.instantiateViewController(withIdentifier: "contentController") as! NavigationVC
//        if let viewController = contentNavController.visibleViewController{
//            if viewController.isKind(of: InquiryVC.self){
//                viewController.performSegue(withIdentifier: "videostreamSegue", sender: nil)
//            }
//        }
        
//        if let topController = UIApplication.topViewController() {
//
//            if topController.isKind(of: IncomingCallVCWithCamera.self){
//                topController.performSegue(withIdentifier: "videostreamSegue", sender: nil)
//            }else  if topController.isKind(of: NavigationVC.self){
//
//                var aNavigationVC : NavigationVC
//                aNavigationVC = topController as! NavigationVC
//
//                aNavigationVC.pushViewController(vc, animated: true)
//            }
//
//        }
        
        let incomingCallVCWithCamera = storyBoard.instantiateViewController(withIdentifier: "IncomingCallVCWithCamera") as! IncomingCallVCWithCamera
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        //appDelegate.window?.rootViewController = incomingCallVCWithCamera
        //self.navigationController?.pushViewController(incomingCallVCWithCamera, animated: false)

        //let navController = UINavigationController()
        let navController = self.application.windows[0].rootViewController as! UINavigationController
        navController.pushViewController(incomingCallVCWithCamera, animated: true)
        appDelegate.window?.rootViewController = navController
        
        //appDelegate.window?.rootViewController = incomingCallVCWithCamera
        
    }
    
    func provider(_ provider: CXProvider, perform action: CXEndCallAction) {
        action.fulfill()
        
        //_ = navigationController?.popViewController(animated: true)
        //NotificationCenter.default.post(name: Notification.Name("pubNubMessage"), object: nil)
    }
    
    func pushRegistry(_ registry: PKPushRegistry, didUpdate pushCredentials: PKPushCredentials, for type: PKPushType) {
        print(pushCredentials.token.map { String(format: "%02.2hhx", $0) }.joined())
    }
    
    //MARK: Firebase Methods
    var applicationStateString: String {
        if UIApplication.shared.applicationState == .active {
            return "active"
        } else if UIApplication.shared.applicationState == .background {
            return "background"
        }else {
            return "inactive"
        }
    }
    
    func requestNotificationAuthorization(application: UIApplication) {
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(options: authOptions, completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
         Messaging.messaging().delegate = self
    }
}

extension UIApplication {
    class func topViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let navigationController = controller as? UINavigationController {
            return topViewController(controller: navigationController.visibleViewController)
        }
        if let tabController = controller as? UITabBarController {
            if let selected = tabController.selectedViewController {
                return topViewController(controller: selected)
            }
        }
        if let presented = controller?.presentedViewController {
            return topViewController(controller: presented)
        }
        return controller
    }
}

@available(iOS 10, *)
extension AppDelegate : UNUserNotificationCenterDelegate {
    // iOS10+, called when presenting notification in foreground
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        NSLog("[UserNotificationCenter] applicationState: \(applicationStateString) willPresentNotification: \(userInfo)")
        //TODO: Handle foreground notification
        
        if let aps = userInfo["aps"] as? NSDictionary {
            if let alert = aps["alert"] as? NSDictionary {
                if let clientId = alert["title"] as? String {
                    if clientId.length > 0{
                        UserDefaults.standard.set(clientId, forKey: "client_id")
                    }
                }
            }
        }
        
        completionHandler([.alert])
    }
    
    // iOS10+, called when received response (default open, dismiss or custom action) for a notification
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        NSLog("[UserNotificationCenter] applicationState: \(applicationStateString) didReceiveResponse: \(userInfo)")
        //TODO: Handle background notification
        if let aps = userInfo["aps"] as? NSDictionary {
            if let alert = aps["alert"] as? NSDictionary {
                if let clientId = alert["title"] as? String {
                    if clientId.length > 0{
                        UserDefaults.standard.set(clientId, forKey: "client_id")
                    }
                }
            }
        }
        
        completionHandler()
    }
}

extension AppDelegate : MessagingDelegate {
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        NSLog("[RemoteNotification] didRefreshRegistrationToken: \(fcmToken)")
        
        if fcmToken.length > 0{
            UserDefaults.standard.set(fcmToken, forKey: "FCMTOKEN")
            
            //MARK: init PubNub after getting FCM Token
            self.pubNubInit()
        }
    }
    
    // iOS9, called when presenting notification in foreground
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        NSLog("[RemoteNotification] applicationState: \(applicationStateString) didReceiveRemoteNotification for iOS9: \(userInfo)")
        if UIApplication.shared.applicationState == .active {
            //TODO: Handle foreground notification
        } else {
            //TODO: Handle background notification
        }
        
        NSLog("[UserNotificationCenter] applicationState: \(applicationStateString) willPresentNotification: \(userInfo)")
        //TODO: Handle foreground notification
        
        if let aps = userInfo["aps"] as? NSDictionary {
            if let alert = aps["alert"] as? NSDictionary {
                if let clientId = alert["title"] as? String {
                    if clientId.length > 0{
                        UserDefaults.standard.set(clientId, forKey: "client_id")
                    }
                }
            }
        }
    }
    
     //MARK : PubNub client update configurations
    func updateClientConfiguration(){
        // Set PubNub Configuration
        self.myConfig!.TLSEnabled = false
        
        let fcmToken = UserDefaults.standard.string(forKey: "FCMTOKEN")
        self.myConfig!.uuid = fcmToken!
        self.myConfig!.origin = "pubsub.pubnub.com"
        self.myConfig!.authKey = self.authKey as String
        
        self.myConfig?.stripMobilePayload = true
        
        // Presence Settings
        self.myConfig!.presenceHeartbeatValue = 120
        self.myConfig!.presenceHeartbeatInterval = 10
        
        // Cipher Key Settings
        //self.client.cipherKey = @"enigma";
        
        // Time Token Handling Settings
        self.myConfig!.keepTimeTokenOnListChange = true
        self.myConfig!.catchUpOnSubscriptionRestore = true
        
        // Messages threshold
        self.myConfig!.requestMessageCountThreshold = 100
    }
    
    func randomString()->NSString{
        let random = arc4random_uniform(74)
        let formatted = NSString(format:"randomString :- %d", random) as NSString
        return formatted
    }
    
    //MARK : PubNub Methods
    func pubNubInit(){
        
        let fcmToken = UserDefaults.standard.string(forKey: "FCMTOKEN")
        //MARK:  Configurations for PubNub
        self.channel1 = String(format: "%@%@", fcmToken!, "-stdby") as NSString
        self.channel2 = "myCh"
        self.channelGroup1 = "myChannelGroup"
        self.authKey  = "myAuthKey"
        self.pubKey   = "pub-c-aef19144-b0cb-4a9e-8e38-bdb3f14ee7b5"
        self.subKey   = "sub-c-e036c610-00f0-11e8-8274-06cc4b902b08"
        
        if self.client == nil {
            
            // Initialize and configure PubNub client instance
            myConfig = PNConfiguration(publishKey: pubKey! as String, subscribeKey: subKey! as String)
            self.updateClientConfiguration()
            // Bind config
            self.client = PubNub.clientWithConfiguration(myConfig!)
            self.printClientConfiguration()
            // Configure logger
            self.client.logger.enabled = true
            self.client.logger.writeToFile = true
            self.client.logger.maximumLogFileSize = (10 * 1024 * 1024)
            self.client.logger.maximumNumberOfLogFiles = 10
            self.client.logger.enableLogLevel(8)
            /**
             Subscription process results arrive to listener which should adopt to PNObjectEventListener protocol
             and registered using:
             */
            self.client.addListener(self)
            //self.client.subscribeToChannels([self.channel1! as String], withPresence: true)
        }
        
        self.client.subscribeToChannels([self.channel1! as String], withPresence: true)
        
         //MARK: - Set State
        self.pubNubSetState()
        self.pubNubGetState()
        
        //self.pubNubChannelsForGroup()

    }
    
    // MARK: PubNub Delegates
    // MARK: PubNub client -  Handle new message from one of channels on which client has been subscribed.
    func client(_ client: PubNub, didReceiveMessage message: PNMessageResult) {
        
        // Handle new message stored in message.data.message
        if message.data.channel != message.data.subscription {
            
            // Message has been received on channel group stored in message.data.subscription.
        }
        else {
            
            // Message has been received on channel stored in message.data.channel.
        }
        
        // Copy PNMessageResult for establise call
        self.pubNubMessage = message
        
//        let messageDict = message.data.message as! NSDictionary
//        print("Received message:\(messageDict.value(forKey: "call_user") as! String)")
        
        print("Received message: \(String(describing: message.data.message))")
        print("Received on channel:  \(message.data.channel)")
        print("Received at timetoken: \(message.data.timetoken)")
        print("Received userMetadata:\(String(describing: message.data.userMetadata))")
        print("Received message data:\(message.data)")
                
        /*
         if ([messageData isKindOfClass:[NSDictionary class]]) {
         
         // It will be better to access cipher key directly, because 'currentConfiguration'
         // make copy of PNConfiguration each time.
         NSString *cipherKey = [client currentConfiguration].cipherKey;
         NSMutableDictionary *messagePayload = [messageData mutableCopy];
         if (cipherKey.length && messagePayload[@"pn_other"]) {
         
         NSError *parseError = nil;
         id decryptedMessageData = [PNAES decrypt:messagePayload[@"pn_other"] withKey:cipherKey
         andError:&parseError];
         if (decryptedMessageData) {
         
         messageData = [NSJSONSerialization JSONObjectWithData:decryptedMessageData
         options:NSJSONReadingAllowFragments
         error:&parseError];
         }
         if (!parseError) {
         
         if (![messageData isKindOfClass:[NSDictionary class]]) {
         
         messagePayload[@"pn_other"] = messageData;
         } else { [messagePayload addEntriesFromDictionary:messageData]; }
         }
         else { /* Handle message decryption and JSON decode. */ }
         }
         // Remove keys for any used push notification provider.
         [messagePayload removeObjectsForKeys:@[@"pn_apns", @"pn_gcm", @"pn_mpns"]];
         messageData = (messagePayload[@"pn_other"]?: messagePayload);
         }
         NSLog(@"Received message: %@", messageData);
         */
//        //Navigate to Incoming Call View Controller after receive a Message
//        let navigationController = self.application.windows[0].rootViewController as! UINavigationController
//        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//        let vc = storyBoard.instantiateViewController(withIdentifier: "IncomingCallVC") as! IncomingCallVC
//        navigationController.pushViewController(vc, animated: true)
        
        // Start Call Ringing
        self.startRingin()
    }
    
    // MARK: PubNub client -  New presence event handling.
    func client(_ client: PubNub, didReceivePresenceEvent event: PNPresenceEventResult) {
        
        // Handle presence event event.data.presenceEvent (one of: join, leave, timeout, state-change).
        if event.data.channel != event.data.subscription {
            
            // Presence event has been received on channel group stored in event.data.subscription.
        }
        else {
            
            // Presence event has been received on channel stored in event.data.channel.
        }
        
        if event.data.presenceEvent != "state-change" {
            
            print("\(String(describing: event.data.presence.uuid)) \"\(event.data.presenceEvent)'ed\"\n" +
                "at: \(event.data.presence.timetoken) on \(event.data.channel) " +
                "(Occupancy: \(event.data.presence.occupancy))");
        }
        else {
            
            print("\(String(describing: event.data.presence.uuid)) changed state at: " +
                "\(event.data.presence.timetoken) on \(event.data.channel) to:\n" +
                "\(String(describing: event.data.presence.state))");
        }
    }
    
// MARK: PubNub client - Handle subscription status change.
    func client(_ client: PubNub, didReceive status: PNStatus) {
        
        print("status.operation: \(String(describing: status.operation.rawValue))")
        
        if status.operation == .subscribeOperation {
            
            // Check whether received information about successful subscription or restore.
            if status.category == .PNConnectedCategory || status.category == .PNReconnectedCategory {
                
                let subscribeStatus: PNSubscribeStatus = status as! PNSubscribeStatus
                if subscribeStatus.category == .PNConnectedCategory {
                    
                    // This is expected for a subscribe, this means there is no error or issue whatsoever.
                    
                    // Select last object from list of channels and send message to it.
                    let targetChannel = client.channels().last!
                    /*client.publish("Hello from the PubNub Swift SDK", toChannel: targetChannel,
                                   compressed: false, withCompletion: { (publishStatus) -> Void in
                                    
                                    if !publishStatus.isError {
                                        
                                        // Message successfully published to specified channel.
                                    }
                                    else {
                                        
                                        /**
                                         Handle message publish error. Check 'category' property to find out
                                         possible reason because of which request did fail.
                                         Review 'errorData' property (which has PNErrorData data type) of status
                                         object to get additional information about issue.
                                         
                                         Request can be resent using: publishStatus.retry()
                                         */
                                    }
                    })*/
                }
                else {
                    
                    /**
                     This usually occurs if subscribe temporarily fails but reconnects. This means there was
                     an error but there is no longer any issue.
                     */
                }
            }
            else if status.category == .PNUnexpectedDisconnectCategory {
                
                /**
                 This is usually an issue with the internet connection, this is an error, handle
                 appropriately retry will be called automatically.
                 */
            }
                // Looks like some kind of issues happened while client tried to subscribe or disconnected from
                // network.
            else {
                
                let errorStatus: PNErrorStatus = status as! PNErrorStatus
                if errorStatus.category == .PNAccessDeniedCategory {
                    
                    /**
                     This means that PAM does allow this client to subscribe to this channel and channel group
                     configuration. This is another explicit error.
                     */
                }
                else {
                    
                    /**
                     More errors can be directly specified by creating explicit cases for other error categories
                     of `PNStatusCategory` such as: `PNDecryptionErrorCategory`,
                     `PNMalformedFilterExpressionCategory`, `PNMalformedResponseCategory`, `PNTimeoutCategory`
                     or `PNNetworkIssuesCategory`
                     */
                }
            }
        }
    }
    
    // MARK: PubNub Handlers
    func pubNubTime(){
        self.client?.timeWithCompletion({ (result, status) -> Void in
            if result?.data != nil {
                //            if let Result = result!.data{
                print("Result from Time: \(result!.data.timetoken)")
            }
            if status != nil{
                self.handleStatus(status: status!)
            }
        })
    }
    
    func pubNubSetState(){
        self.client.setState(["status" : "Available"], forUUID: self.client.uuid(), onChannel: self.channel1! as String, withCompletion: { (status) in
            self.handleStatus(status: status)
        })
    }
    
    func pubNubGetState(){
        
        self.client.stateForUUID(self.client.uuid(), onChannel: self.channel1! as String, withCompletion: { (result, status) -> Void in
            if result != nil{
                print("^^^^ Loaded state \(result!.data.state) for channel \(self.channel1!)")
                
            }
            if status != nil{
                self.handleStatus(status: status!)
            }
        })
    }
    func pubNubHistory(){
        // History
        self.client.historyForChannel(channel1! as String, withCompletion: { (result, status) -> Void in
            // For completion blocks that provide both result and status parameters, you will only ever
            // have a non-nil status or result.
            
            // If you have a result, the data you specifically requested (in this case, history response) is available in result.data
            // If you have a status, error or non-error status information is available regarding the call.
            
            if result != nil{
                // As a result, this contains the messages, start, and end timetoken in the data attribute
                
                print("Loaded history data: \(result!.data.messages) with start \(result!.data.start) and end \(result!.data.end)")
            }
            if status != nil{
                // As a status, this contains error or non-error information about the history request, but not the actual history data I requested.
                // Timeout Error, PAM Error, etc.
                self.handleStatus(status: status!)
            }
        })
       
    }
    
    func pubNubUnsubscribeFromChannels(){
        self.client.unsubscribeFromChannels([channelGroup1! as String], withPresence: true)
    }
    
    func pubNubSubscribeToChannelGroup(){
        self.client.subscribeToChannelGroups([channelGroup1! as String], withPresence: false)
        /*
         [self.client subscribeToChannelGroups:@[_channelGroup1] withPresence:YES clientState:@{@"foo":@"bar"}];
         */
    }

    func pubNubSubscribeToChannels() {
         //[self.client subscribeToChannels:@[_channel1] withPresence:YES clientState:@{_channel1:@{@"foo":@"bar"}}];
    }
    
    func pubNubSubscribeToPresence() {
        self.client.subscribeToPresenceChannels([channel1! as String])
    }
    
    func pubNubUnsubFromPresence() {
        self.client.unsubscribeFromPresenceChannels([channel1! as String])
    }
    
    func pubNubGlobalHereNow(){
        self.client.hereNowWithCompletion({ (result, status) -> Void in
            
            if result != nil{
                print("^^^^ Loaded Global hereNow data: channels: \(result!.data.channels), total channels: \(result!.data.totalChannels), total occupancy: \(result!.data.totalOccupancy)")
            }
            if status != nil{
                self.handleStatus(status: status!)
            }
            
        })
        // If you want to control the 'verbosity' of the server response -- restrict to (values are additive):
        
        // Occupancy                : PNHereNowOccupancy
        // Occupancy + UUID         : PNHereNowUUID
        // Occupancy + UUID + State : PNHereNowState
        
        self.client.hereNowWithVerbosity(PNHereNowVerbosityLevel.occupancy, completion: { (result, status) -> Void in
            
            if result != nil{
                print("^^^^ Loaded Global hereNow data: channels: \(result!.data.channels), total channels: \(result!.data.totalChannels), total occupancy: \(result!.data.totalOccupancy)");
            }
            if status != nil{
                self.handleStatus(status: status!)
            }
        })
    }
    
    func pubNubHereNowForChannelGroups(){
        /*
         [self.client hereNowForChannelGroup:<#(NSString *)group#> withCompletion:<#(PNChannelGroupHereNowCompletionBlock)block#>];
         [self.client hereNowForChannelGroup:<#(NSString *)group#> withVerbosity:<#(PNHereNowVerbosityLevel)level#> completion:<#(PNChannelGroupHereNowCompletionBlock)block#>];
         */
    }
    
    func pubNubHereNowForChannel(){
        self.client.hereNowForChannel(self.channel1! as String, withCompletion: { (result, status) -> Void in
            
            if result != nil{
                print("^^^^ Loaded hereNowForChannel data: occupancy: \(result!.data.occupancy), uuids: \(result!.data.uuids ?? "")")
                
            }
            if status != nil{
                self.handleStatus(status: status!)
            }
            
        })
        
        // If you want to control the 'verbosity' of the server response -- restrict to (values are additive):
        
        // Occupancy                : PNHereNowOccupancy
        // Occupancy + UUID         : PNHereNowUUID
        // Occupancy + UUID + State : PNHereNowState
        
        
        
        self.client.hereNowForChannel(channel1! as String, withVerbosity:  PNHereNowVerbosityLevel.state, completion: { (result, status) -> Void in
            //PNHereNowVerbosityLevel.State is not PNHereNowState
            if result != nil{
                print("^^^^ Loaded hereNowForChannel data: occupancy: \(result!.data.occupancy), uuids: \(String(describing: result!.data.uuids))")
            }
            if status != nil{
                self.handleStatus(status: status!)
            }
        })
    }
    
    func pubNubWhereNow(){
        self.client.whereNowUUID("123456", withCompletion: { (result, status) -> Void in
            if result?.data != nil {
                //            if let Result = result!.data{
                print("^^^^ Loaded whereNow data: \(result!.data.channels)")
            }
            if status != nil{
                self.handleStatus(status: status!)
            }
        })
    }
    
    func pubNubCGAdd(){
        //weak var weakSelf = self
        
        self.client.addChannels([channel1! as String, channel2! as String], toGroup: channelGroup1! as String, withCompletion: { (status) -> Void in
            if(!status.isError){
                print("^^^^ CGAdd Request Succeeded")
            }
            else{
                print("^^^^ CGAdd Subscribe requet did not succeed. All Subscribe operations will autorety when possible")
                self.handleStatus(status: status)
            }
        })
    }
    
    func pubNubChannelsForGroup(){
        self.client.channelsForGroup(channelGroup1! as String, withCompletion: { (result, status) -> Void in
            if result?.data != nil {
                //            if let Result = result.data{
                print("^^^^ Loaded all channels : \(result!.data.channels) for group \(self.channelGroup1!)");
            }
            if status != nil{
                self.handleStatus(status: status!)
            }
        })
    }
    
    func pubNubCGRemoveSomeChannels(){
        
        self.client.removeChannels([channel2! as String], fromGroup: channelGroup1! as String, withCompletion: { (status) -> Void in
            if(!status.isError){
                print("^^^^CG Remove some channgels request succeeded at time token ")
            }
            else{
                print("^^^^CG Remove some channels request did not succeed. All Subscribe operations will autoretry when possible")
                self.handleStatus(status: status)
            }
        })
    }
    
    
    func pubNubCGRemoveAllChannels(){
        self.client.removeChannelsFromGroup(channelGroup1! as String, withCompletion: { (status) -> Void in
            if(!status.isError){
                print("^^^^ CG Remove All Channels request succeeded")
            }
            else{
                print("^^^^ CG Remove All Channels did not succeed. All Subscribe operations will autorety when possible.")
                self.handleStatus(status: status)
            }
        })
    }
    
    func pubNubSizeOfMessage(){
        self.client.sizeOfMessage("Connected! I'm here!", toChannel: channel1! as String, withCompletion: { (size) -> Void in
            print("^^^^ Message size: \(size)")
        })
    }
    
    func pubNubAESDecrypt(){
        /*
         [PNAES decrypt:<#(NSString *)object#> withKey:<#(NSString *)key#>];
         [PNAES decrypt:<#(NSString *)object#> withKey:<#(NSString *)key#> andError:<#(NSError *__autoreleasing *)error#>];
         */
    }
    
    func pubNubAESEncrypt(){
        /*
         [PNAES encrypt:<#(NSData *)data#> withKey:<#(NSString *)key#>];
         [PNAES encrypt:<#(NSData *)data#> withKey:<#(NSString *)key#> andError:<#(NSError *__autoreleasing *)error#>];
         */
    }
    
    func pubNubAddPushNotifications(){
        /*
         self.client.addPushNotificationsOnChannels((NSArray *)channels withDevicePushToken:(NSData *)pushToken andCompletion:(PNPushNotificationsStateModificationCompletionBlock)block)
         */
    }
    
    func pubNubRemovePushNotification(){
        /*
         self.client.removePushNotificationsFromChannels((NSArray *)channels withDevicePushToken:(NSData *)pushToken andCompletion:(PNPushNotificationsStateModificationCompletionBlock)block)
         */
    }
    
    func pubNubRemoveAllPushNotifications(){
        /*
         self.client.removeAllPushNotificationsFromDeviceWithPushToken((NSData *)pushToken andCompletion:(PNPushNotificationsStateModificationCompletionBlock)block)
         */
    }
    
    func pubNubGetAllPushNotifications(){
        /*
         self.client.pushNotificationEnabledChannelsForDeviceWithPushToken((NSData *)pushToken andCompletion:(PNPushNotificationsStateAuditCompletionBlock)block)
         */
    }
    
    func handleStatus(status : PNStatus){
        //  Two types of status events are possible. Errors, and non-errors. Errors will prevent normal operation of your app.
        //
        //    If this was a subscribe or presence PAM error, the system will continue to retry automatically.
        //    If this was any other operation, you will need to manually retry the operation.
        //
        //    You can always verify if an operation will auto retry by checking status.willAutomaticallyRetry
        //    If the operation will not auto retry, you can manually retry by calling [status retry]
        //    Retry attempts can be cancelled via [status cancelAutomaticRetry]
        
        if(status.isError){
            handleErrorStatus(status: status as! PNErrorStatus)
        }
        else{
            handleNonErrorStatus(status: status)
        }
    }
    
    func handleErrorStatus(status : PNErrorStatus){
        print("Debug: \(status.debugDescription)")
        print("handleErrorStatus: PAM Error: for resource Will Auto Retry?: \(status.willAutomaticallyRetry)")
        
        if (status.category == PNStatusCategory.PNAccessDeniedCategory) {
            self.handlePAMError(status: status)
        }
        else if (status.category == PNStatusCategory.PNDecryptionErrorCategory) {
            
            print("Decryption error. Be sure the data is encrypted and/or encrypted with the correct cipher key.")
            print("You can find the raw data returned from the server in the status.data attribute: \(status.errorData)")
        }
        else if (status.category == PNStatusCategory.PNMalformedResponseCategory) {
            
            print("We were expecting JSON from the server, but we got HTML, or otherwise not legal JSON.")
            print("This may happen when you connect to a public WiFi Hotspot that requires you to auth via your web browser first,")
            print("or if there is a proxy somewhere returning an HTML access denied error, or if there was an intermittent server issue.")
        }
            
        else if (status.category == PNStatusCategory.PNTimeoutCategory) {
            
            print("For whatever reason, the request timed out. Temporary connectivity issues, etc.")
        }
            
        else {
            // Aside from checking for PAM, this is a generic catch-all if you just want to handle any error, regardless of reason
            // status.debugDescription will shed light on exactly whats going on
            
            print("Request failed... if this is an issue that is consistently interrupting the performance of your app,");
            print("email the output of debugDescription to support along with all available log info: \(status.debugDescription)");
        }
    }
    
    func handleNonErrorStatus(status : PNStatus){
        // This method demonstrates how to handle status events that are not errors -- that is,
        // status events that can safely be ignored, but if you do choose to handle them, you
        // can get increased functionality from the client
        
        if (status.category == PNStatusCategory.PNAcknowledgmentCategory) {
            print("^^^^ Non-error status: ACK")
            
            
            // For methods like Publish, Channel Group Add|Remove|List, APNS Add|Remove|List
            // when the method is executed, and completes, you can receive the 'ack' for it here.
            // status.data will contain more server-provided information about the ack as well.
            
        }
        if (status.operation == PNOperationType.subscribeOperation) {
            
            let subscriberStatus: PNSubscribeStatus = status as! PNSubscribeStatus
            // Specific to the subscribe loop operation, you can handle connection events
            // These status checks are only available via the subscribe status completion block or
            // on the long-running subscribe loop listener didReceiveStatus
            
            // Connection events are never defined as errors via status.isError
            
            if (status.category == PNStatusCategory.PNDisconnectedCategory) {
                // PNDisconnect happens as part of our regular operation
                // No need to monitor for this unless requested by support
                print("^^^^ Non-error status: Expected Disconnect, Channel Info: \(subscriberStatus.subscribedChannels)");
            }
                
            else if (status.category == PNStatusCategory.PNUnexpectedDisconnectCategory) {
                // PNUnexpectedDisconnect happens as part of our regular operation
                // This event happens when radio / connectivity is lost
                
                print("^^^^ Non-error status: Unexpected Disconnect, Channel Info: \(subscriberStatus.subscribedChannels)")
            }
                
            else if (status.category == PNStatusCategory.PNConnectedCategory) {
                
                // Connect event. You can do stuff like publish, and know you'll get it.
                // Or just use the connected event to confirm you are subscribed for UI / internal notifications, etc
                
                print("^^^^ Non-error status: Connected, Channel Info: \(subscriberStatus.subscribedChannels)");
                self.pubNubPublish()
                
            }
            else if (status.category == PNStatusCategory.PNReconnectedCategory) {
                
                // PNUnexpectedDisconnect happens as part of our regular operation
                // This event happens when radio / connectivity is lost
                
                print("^^^^ Non-error status: Reconnected, Channel Info: \(subscriberStatus.subscribedChannels)")
                
            }
            
        }
    }
    
    func handlePAMError(status : PNErrorStatus){
        let pamResourceName = status.errorData.channels != nil ? status.errorData.channels.first : status.errorData.channelGroups.first
        let pamResourceType = status.errorData.channels != nil ? "channel" : "channel-groups"
        
        print("PAM error on \(pamResourceType) \(pamResourceName ?? "")")
        
        if status.operation == PNOperationType.subscribeOperation {
            
            if (pamResourceType == "channel") {
                print("^^^^ Unsubscribing from \(pamResourceName ?? "")")
                reconfigOnPamError(status: status)
            } else {
                client?.unsubscribeFromChannelGroups([pamResourceName!], withPresence: true)
                //                client?.unsubscribe(fromChannelGroups: [pamResourceName], withPresence: true)
            }
        } else if status.operation == PNOperationType.publishOperation {
            
            print("^^^^ Error publishing with authKey: \(authKey!) to channel \(pamResourceName ?? "").")
            print("^^^^ Setting auth to an authKey that will allow for both sub and pub")
            
            reconfigOnPamError(status: status)
        }
    }
    
    func reconfigOnPamError(status : PNErrorStatus){
        // If this is a subscribe PAM error
        
        if (status.operation == PNOperationType.subscribeOperation) {
            
            let subscriberStatus : PNSubscribeStatus = status as! PNSubscribeStatus
            
            let currentChannels: NSArray = subscriberStatus.subscribedChannels as NSArray;
            let currentChannelGroups: NSArray = subscriberStatus.subscribedChannelGroups as NSArray;
            
            self.myConfig!.authKey = "myAuthKey"
            
            self.client?.copyWithConfiguration(myConfig!, completion: { (client) -> Void in
                
            })
            
            self.client?.subscribeToChannels(currentChannels as! [String], withPresence: false)
            self.client?.subscribeToChannelGroups(currentChannelGroups as! [String] , withPresence: false)
        }
    }
    
    func pubNubPublish(){
//        self.client.publish("Connected! I'm here!", toChannel: channel1! as String, withCompletion: { (status) -> Void in
//            if(!status.isError){
//                print("Message sent at TT: \(status.data.timetoken)")
//            }
//            else{
//                self.handleStatus(status: status)
//            }
//        })
        
        self.client.publish("Connected! I'm here!", toChannel: self.channel1! as String, compressed: true, withCompletion: { (status) -> Void in
            
            if(!status.isError){
                print("Message sent at TT: \(status.data.timetoken)")
            }
            else{
                self.handleStatus(status: status)
            }
        })
        
        /*
         [self.client publish:<#(id)message#> toChannel:<#(NSString *)channel#> compressed:<#(BOOL)compressed#> withCompletion:<#(PNPublishCompletionBlock)block#>];
         [self.client publish:<#(id)message#> toChannel:<#(NSString *)channel#> withCompletion:<#(PNPublishCompletionBlock)block#>];
         [self.client publish:<#(id)message#> toChannel:<#(NSString *)channel#> storeInHistory:<#(BOOL)shouldStore#> withCompletion:<#(PNPublishCompletionBlock)block#>];
         [self.client publish:<#(id)message#> toChannel:<#(NSString *)channel#> storeInHistory:<#(BOOL)shouldStore#> compressed:<#(BOOL)compressed#> withCompletion:<#(PNPublishCompletionBlock)block#>];
         [self.client publish:<#(id)message#> toChannel:<#(NSString *)channel#> mobilePushPayload:<#(NSDictionary *)payloads#> withCompletion:<#(PNPublishCompletionBlock)block#>];
         [self.client publish:<#(id)message#> toChannel:<#(NSString *)channel#> mobilePushPayload:<#(NSDictionary *)payloads#> compressed:<#(BOOL)compressed#> withCompletion:<#(PNPublishCompletionBlock)block#>];
         [self.client publish:<#(id)message#> toChannel:<#(NSString *)channel#> mobilePushPayload:<#(NSDictionary *)payloads#> storeInHistory:<#(BOOL)shouldStore#> withCompletion:<#(PNPublishCompletionBlock)block#>];
         [self.client publish:<#(id)message#> toChannel:<#(NSString *)channel#> mobilePushPayload:<#(NSDictionary *)payloads#> storeInHistory:<#(BOOL)shouldStore#> compressed:<#(BOOL)compressed#> withCompletion:<#(PNPublishCompletionBlock)block#>];
         */
    }
    
    func printClientConfiguration(){
        // Get PubNub Options
        print("TLSEnabled: \(self.myConfig!.TLSEnabled) ")
        print("Origin: \(self.myConfig!.origin)")
        //print("authKey: \(self.myConfig!.authKey!)")
        print("UUID: \(self.myConfig!.uuid)");
        
        // Time Token Handling Settings
        print("keepTimeTokenOnChannelChange: \(self.myConfig!.keepTimeTokenOnListChange)" )
        //#^ //n
        //        print("resubscribeOnConnectionRestore: \(self.myConfig?.restoreSubscription)")
        //#^ y/n
        print("catchUpOnSubscriptionRestore: \(self.myConfig!.catchUpOnSubscriptionRestore)")
        
        // Get Presence Options
        print("Heartbeat value: \(self.myConfig!.presenceHeartbeatValue)")
        print("Heartbeat interval: \(self.myConfig!.presenceHeartbeatInterval)")
        
        // Get CipherKey
        print("Cipher key: \(String(describing: self.myConfig!.cipherKey))")
    }
    
    // MARK: Application Methods
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        // 1. Convert device token to string
        let tokenParts = deviceToken.map { data -> String in
            return String(format: "%02.2hhx", data)
        }
        let token = tokenParts.joined()
        // 2. Print device token to use for PNs payloads
        print("Device Token: \(token)")
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("APNs registration failed: \(error)")
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        let email = UserDefaults.standard.string(forKey: "email")
        
        if email?.length == 0{
            return;
        }
        print(email!)
        offline(email:email!)
        sleep(3)
    }
    
    //MARK:Service calling
    func offline(email:String)
    {
        var dict = Dictionary<String, String>()
        
        dict = ["email": email]
        
        print(dict)
        _ = WebserviceCall_Swift.shared.initWithDelegate(delegate: self, callBack: #selector(self.ServiceHandler(sender:)))
        DispatchQueue.global(qos: .background).async{
            WebserviceCall_Swift.shared.serviceCall(parameterDict: dict , url : "offline")
        }
    }
    
    //MARK: Service handler
    @objc func ServiceHandler(sender:AnyObject)
    {
        DispatchQueue.main.async
            {
               // MBProgressHUD.hide(for: self.view, animated: true)
                if sender.isKind(of: NSString.self)
                {
                    Singleton.shared.error = sender as! String
                    if Singleton.shared.error == "No Internet Connection"
                    {
                        AppDelegate.showAlert(message: "No Internet Connection", title: "Connection Failed!")
                    }
                    else
                    {
                        return
                    }
                }
                
                if sender.isKind(of: NSArray.self)
                {
                    let arrResult = sender as! NSArray
                    let json = arrResult.object(at: 0) as! NSDictionary
                    let serviceStr = arrResult.object(at: 1) as! String
                    
                    if serviceStr == "offline"
                    {
                        if json.object(forKey: "email") as? String != nil
                        {
                            let delItemInfo = json.object(forKey: "online_status") as! Bool
                            print(delItemInfo)
                            
                        }else
                        {
                            let err = json.object(forKey: "message") as! String
                            AppDelegate.showAlert(message: err, title: "Info")
                        }
                    }
                }
        }
    }
    
//    class func get -> PNMessageResult{
//        return self.pubNubMessage
//    }
    
    //MARK: Alert method
    class func showAlert(message:String , title : String)
    {
        let topWindow = UIWindow(frame: UIScreen.main.bounds)
        topWindow.rootViewController = UIViewController()
        topWindow.windowLevel = UIWindow.Level.alert + 1
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "confirm"), style: .cancel, handler: {(_ action: UIAlertAction) -> Void in
            // continue your work
            // important to hide the window after work completed.
            // this also keeps a reference to the window until the action is invoked.
            topWindow.isHidden = true
        }))
        topWindow.makeKeyAndVisible()
        topWindow.rootViewController?.present(alert, animated: true, completion: {})
    }

}

