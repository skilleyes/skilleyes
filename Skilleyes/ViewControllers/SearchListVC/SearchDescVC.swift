//
//  SearchDescVC.swift
//  Skilleyes
//
//  Created by Mac HD on 26/11/18.
//  Copyright © 2018 Mac HD. All rights reserved.
//

import UIKit

class SearchDescVC: UIViewController, UITextViewDelegate {

    //Outlets
    @IBOutlet weak var vewMain: UIView!
    @IBOutlet var txtVewDesc: UITextView!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblPrice: UILabel!
    @IBOutlet var btnSend: UIButton!
    
    let STATUSBAR_COLOR = #colorLiteral(red: 0.8745098039, green: 0.8784313725, blue: 0.8823529412, alpha: 1)
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //setup status bar
        self.setNeedsStatusBarAppearanceUpdate()
        setStatusBarBackgroundColor(color: STATUSBAR_COLOR)
        
        changeLanguage()
        
        self.shadowView(view: vewMain)
    }
    
    func shadowView(view:UIView)
    {
        // corner radius
        view.layer.cornerRadius = 15
            
        // shadow
        view.layer.shadowColor = UIColor.darkGray.cgColor
        view.layer.shadowOffset = CGSize(width: 3, height: 3)
        view.layer.shadowOpacity = 0.7
        view.layer.shadowRadius = 2.0
    }
    
    //MARK: Status bar text color
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    func setStatusBarBackgroundColor(color:UIColor)  {
        let statusBar = (UIApplication.shared.value(forKey: "statusBarWindow") as AnyObject).value(forKey: "statusBar") as? UIView
        statusBar?.backgroundColor = color
    }
    
    //MARK: app language method
    func changeLanguage()
    {
        if (UserDefaults.standard.string(forKey: "Germany") != nil)
        {
            btnSend.setTitle("Anfrage schiken", for: .normal)
            
        }else if (UserDefaults.standard.string(forKey: "America") != nil)
        {
            btnSend.setTitle("Send request", for: .normal)
        }
    }

    //MARK: action method
    @IBAction func actionBack(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionSndReq(_ sender: Any)
    {
        let objLoginVC:SearchListVC = storyboard?.instantiateViewController(withIdentifier: "SearchListVC") as! SearchListVC
        self.navigationController?.pushViewController(objLoginVC, animated: false)
    }
    

}
