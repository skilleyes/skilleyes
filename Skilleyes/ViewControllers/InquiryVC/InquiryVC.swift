//
//  InquiryVC.swift
//  Skilleyes
//
//  Created by Mac HD on 22/11/18.
//  Copyright � 2018 Mac HD. All rights reserved.
//

import UIKit

class InquiryVC: UIViewController, UITextViewDelegate {

    //Outlets
    @IBOutlet weak var txtVewDesc: UITextView!
    @IBOutlet weak var btnSnd: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    
    //Varioables
    let STATUSBAR_COLOR = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    var hud: MBProgressHUD = MBProgressHUD()
    var strDeviceId : String!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        changeLanguage()
        
        //setup status bar
        self.setNeedsStatusBarAppearanceUpdate()
        setStatusBarBackgroundColor(color: STATUSBAR_COLOR)
        
        txtVewDesc.layer.cornerRadius = 10
        txtVewDesc.clipsToBounds = true
    }
    
    //MARK: Status bar text color
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    func setStatusBarBackgroundColor(color:UIColor)  {
        let statusBar = (UIApplication.shared.value(forKey: "statusBarWindow") as AnyObject).value(forKey: "statusBar") as? UIView
        statusBar?.backgroundColor = color
    }
    
    //MARK: app language method
    func changeLanguage()
    {
        if (UserDefaults.standard.string(forKey: "Germany") != nil)
        {
            lblTitle.text = "Anfrage"
            lblSubTitle.text = "Ein ticket erstellen"
            
            txtVewDesc.text = "Beschreibung"
            txtVewDesc.textColor = UIColor.lightGray
            
            btnSnd.setTitle("Anfrage schiken", for: .normal)
            
        }else if (UserDefaults.standard.string(forKey: "America") != nil)
        {
            lblTitle.text = "Inquiry"
            lblSubTitle.text = "Create a ticket"
            
            txtVewDesc.text = "Description"
            txtVewDesc.textColor = UIColor.lightGray
            
            btnSnd.setTitle("Send request", for: .normal)
        }
    }
    
    //MARK: textview methods
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if textView.text.isEmpty {
            if (UserDefaults.standard.string(forKey: "Germany") != nil)
            {
                textView.text = "Beschreibung"
                textView.textColor = UIColor.lightGray
            }else if (UserDefaults.standard.string(forKey: "America") != nil)
            {
                textView.text = "Description"
                textView.textColor = UIColor.lightGray
            }
        }
    }
    
    //MARK: action method
    @IBAction func actionMenu(_ sender: Any)
    {
        self.view.endEditing(true)
        self.frostedViewController.view.endEditing(true)
        self.frostedViewController.presentMenuViewController()
    }
    
    @IBAction func actionSndReq(_ sender: Any)
    {
        if (txtVewDesc.text == "") {
            AppDelegate.showAlert(message: "Please enter description", title: "Message")
        }else {
            let strEmail = UserDefaults.standard.string(forKey: "email")
            if strEmail!.length == 0 {
                return;
            }
            
            self.deviceDetail(email: strEmail!)
        }
    }

    //MARK: Service calling
    //MARK: inquiry Service calling
    func inquiry()
    {
        hud = MBProgressHUD.showAdded(to: self.view, animated: true)
        hud.mode = MBProgressHUDMode.indeterminate
        hud.label.text = "Loading..."
        
        if (UserDefaults.standard.string(forKey: "deviceId") != nil)
        {
            self.strDeviceId = UserDefaults.standard.string(forKey: "deviceId")!
            print(self.strDeviceId)
        }else {
            self.strDeviceId = ""
        }
        
        print(UserDefaults.standard.value(forKey: "UserInfo") as Any)
        
        var dict = Dictionary<String, String>()
        
        let  clientId = UserDefaults.standard.string(forKey: "client_id")
        self.strDeviceId = UserDefaults.standard.string(forKey: "device_id")
        
        dict = ["client_id": "\(clientId!)","device_id": self.strDeviceId,"inquiry_description": "\(txtVewDesc.text!)"]
        
        print(dict)
        _ = WebserviceCall_Swift.shared.initWithDelegate(delegate: self, callBack: #selector(self.ServiceHandler(sender:)))
        DispatchQueue.global(qos: .background).async{
            WebserviceCall_Swift.shared.serviceCall(parameterDict: dict , url : "inquiry")
        }
    }
    
    //MARK: device Service calling
    func deviceDetail(email: String)
    {
        hud = MBProgressHUD.showAdded(to: self.view, animated: true)
        hud.mode = MBProgressHUDMode.indeterminate
        hud.label.text = "Loading..."
        
        var dict = Dictionary<String, String>()
        
        var deviceType = UIDevice.current.modelName
        print("device name--",deviceType)
        
        let bounds = UIScreen.main.bounds
        let width = bounds.size.width
        let height = bounds.size.height
        
        print("device screen width--",width)
        print("device screen height--",height)
        
        let systemVersion = UIDevice.current.systemVersion
        print("os version--",systemVersion)
        
        if (deviceType == ""){
            deviceType = "simulator"
        }
        dict = ["email": "\(email)", "height": "\(height)", "width": "\(width)", "os_version": "\(systemVersion)", "api_level": "22", "device": "iOS", "model": deviceType, "product": "iPone"]
        
        print(dict)
        _ = WebserviceCall_Swift.shared.initWithDelegate(delegate: self, callBack: #selector(self.ServiceHandler(sender:)))
        DispatchQueue.global(qos: .background).async{
            WebserviceCall_Swift.shared.serviceCall(parameterDict: dict , url : "device")
        }
    }
    
    //MARK: Service handler
    @objc func ServiceHandler(sender:AnyObject)
    {
        DispatchQueue.main.async
            {
                MBProgressHUD.hide(for: self.view, animated: true)
                if sender.isKind(of: NSString.self)
                {
                    Singleton.shared.error = sender as! String
                    if Singleton.shared.error == "No Internet Connection"
                    {
                        AppDelegate.showAlert(message: "No Internet Connection", title: "Connection Failed!")
                    }
                    else
                    {
                        return
                    }
                }
                
                if sender.isKind(of: NSArray.self)
                {
                    let arrResult = sender as! NSArray
                    let json = arrResult.object(at: 0) as! NSDictionary
                    let serviceStr = arrResult.object(at: 1) as! String
                    
                    if serviceStr == "inquiry"
                    {
                        if json.object(forKey: "stream_name") as? String != nil
                        {
                            let streamName = json.object(forKey: "stream_name") as! String
                            print(streamName)
                            UserDefaults.standard.setValue(streamName, forKey: "stream_name")
                            
                            self.txtVewDesc.text = ""
                            AppDelegate.showAlert(message: "Inquiry submit successfully", title: "Info")
                        }else
                        {
                            let err = json.object(forKey: "message") as? String
                            AppDelegate.showAlert(message: err ?? "", title: "Info")
                        }
                    }else  if serviceStr == "device"{
                        
                        //if json.object(forKey: "id") as? String != nil
                        if let deviceId = json["id"] as? NSNumber
                        {
                            //let deviceId = json.object(forKey: "id")
                            UserDefaults.standard.setValue(deviceId, forKey: "device_id")
                            self.inquiry()
                        }else{
                            let err = json.object(forKey: "message") as? String
                            AppDelegate.showAlert(message: err ?? "", title: "Info")
                        }
                    }
                }
        }
    }

}
