//
//  SideDrawerVC.swift
//  Augahi
//
//  Created by Pixlr IT on 29/08/18.
//  Copyright © 2018 Pixlr IT. All rights reserved.
//

import UIKit

class SideDrawerVC: UIViewController ,UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet weak var Side_table: UITableView!
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet var vewHeader: UIView!
    @IBOutlet var lblMenu: UILabel!
    
    var arrTitle = NSArray()
    let STATUSBAR_COLOR = #colorLiteral(red: 0.05882352941, green: 0.2274509804, blue: 0.3607843137, alpha: 1)
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        changeLanguage()
        
        //setup status bar
        self.setNeedsStatusBarAppearanceUpdate()
        setStatusBarBackgroundColor(color: STATUSBAR_COLOR)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        changeLanguage()
        
    }
    
    func changeLanguage()
    {
        if (UserDefaults.standard.string(forKey: "Germany") != nil)
        {
            arrTitle = [["id":"1","title":"Meine Fähigkeiten","image":"user"],
                        ["id":"2","title":"Brieftasche","image":"wallet"],
                        ["id":"3","title":"Einstellungen","image":"settings"],
                        ["id":"4","title":"Impressum","image":"info"]]
            
        }else if (UserDefaults.standard.string(forKey: "America") != nil)
        {
            arrTitle = [["id":"1","title":"mySkills","image":"user"],
                        ["id":"2","title":"Wallet","image":"wallet"],
                        ["id":"3","title":"Settings","image":"settings"],
                        ["id":"4","title":"Impressum","image":"info"]]
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setStatusBarBackgroundColor(color:UIColor)  {
        let statusBar = (UIApplication.shared.value(forKey: "statusBarWindow") as AnyObject).value(forKey: "statusBar") as? UIView
        statusBar?.backgroundColor = color
    }
    
    @IBAction func actionClose(_ sender: Any)
    {
        let navigationController = self.storyboard?.instantiateViewController(withIdentifier: "contentController") as! NavigationVC
        self.frostedViewController.contentViewController = navigationController
        self.frostedViewController.hideMenuViewController()
    }
    
    @objc func panGestureRecognizer(sender: UIPanGestureRecognizer) {
        // Dismiss keyboard (optional)
        self.view.endEditing(true)
        self.frostedViewController.view.endEditing(true)
        
        // Present the view controller
        self.frostedViewController.panGestureRecognized(sender)
    }
    
    //MARK: TableView delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.arrTitle.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        var cell = tableView.dequeueReusableCell(withIdentifier: "sidecell") as? sidecell
        if cell == nil {
            let tempArray = Bundle.main.loadNibNamed("sidecell", owner: self, options: nil)
            cell = tempArray?.first  as? sidecell
        }
        
        let DictUser = self.arrTitle[indexPath.row] as! NSDictionary
        print(DictUser)

        let title = String.init(format: "%@", DictUser.object(forKey: "title") as! CVarArg)
        
        if (title == "Wallet"){
            cell?.lblLine.isHidden = false
        }else {
            cell?.lblLine.isHidden = true
        }
        
        cell?.lblTitle.text = String.init(format: "%@", DictUser.object(forKey: "title") as! CVarArg)

        cell?.imgLogo.image = UIImage(named: DictUser.object(forKey: "image") as! String)
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
        let navigationController = self.storyboard?.instantiateViewController(withIdentifier: "contentController") as! NavigationVC
        
        if (indexPath.row == 0)
        {
            print("myskils")
            let secondViewController : MySkillsVC = self.storyboard?.instantiateViewController(withIdentifier: "MySkillsVC") as! MySkillsVC
            navigationController.viewControllers = [secondViewController]
        }
        else if (indexPath.row == 1)
        {
            print("walet")
             let secondViewController : WalletVC = self.storyboard?.instantiateViewController(withIdentifier: "WalletVC") as! WalletVC
             navigationController.viewControllers = [secondViewController]
            
        }else if (indexPath.row == 2)
        {
            print("seting")
            let secondViewController : SettingVC = self.storyboard?.instantiateViewController(withIdentifier: "SettingVC") as! SettingVC
            navigationController.viewControllers = [secondViewController]
            
        }else if (indexPath.row == 3)
        {
            print("impressum")
//            let secondViewController : ServiceFormVC = self.storyboard?.instantiateViewController(withIdentifier: "ServiceFormVC") as! ServiceFormVC
//            navigationController.viewControllers = [secondViewController]
            
        }
        
        self.frostedViewController.contentViewController = navigationController
        self.frostedViewController.hideMenuViewController()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 60
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        
    }
    

}
