//
//  IncomingCallVCWithCamera.swift
//  Skilleyes
//
//  Created by Satya on 20/01/19.
//  Copyright © 2019 Mac HD. All rights reserved.
//

import UIKit
import AudioToolbox
import AVFoundation
import CallKit
import PushKit

class IncomingCallVCWithCamera: UIViewController, CXProviderDelegate, PKPushRegistryDelegate, ARDAppClientDelegate, RTCEAGLVideoViewDelegate {
    
    //Variables
    let STATUSBAR_COLOR = #colorLiteral(red: 0, green: 0.5843137255, blue: 0.9568627451, alpha: 1)
    
    // Web RTC stuff's
    var client : ARDAppClient!
    var roomUrl : String = ""
    var roomName : String = ""
    var localVideoTrack : RTCVideoTrack?
    var  remoteVideoTrack : RTCVideoTrack?
    var localVideoSize : CGSize?
    //var remoteVideoSize : CGSize
    var isZoom : ObjCBool? //used for double tap remote view
    //togle button parameter
    var isAudioMute : Bool?
    var isVideoMute : Bool?
    @IBOutlet weak var remoteView : RTCEAGLVideoView?
    @IBOutlet weak var localView : RTCEAGLVideoView?
    
    @IBOutlet weak var buttonContainerView : UIView?
    
    var  SERVER_HOST_URL  =    "https://dev.ios.skilleyes.com"   //"https://api.core.skilleyes.com/" //"https://appr.tc"
    
    //---- Lib Jingle ---
    var connectionFactory: RTCPeerConnectionFactory = RTCPeerConnectionFactory()
    var iceServers: [RTCICEServer] = []
    var peerConnection: RTCPeerConnection?
    //----
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //setup status bar
        self.setNeedsStatusBarAppearanceUpdate()
        setStatusBarBackgroundColor(color: STATUSBAR_COLOR)
        
        self.isZoom = false
        self.isAudioMute = false
        self.isVideoMute = false
        
        //RTCEAGLVideoViewDelegate provides notifications on video frame dimensions
        self.remoteView?.delegate = self
        self.localView?.delegate = self
        
//        self.roomUrl = NSString(format: "%@/r/%@", SERVER_HOST_URL, "715664900") as String
//        self.roomName = "715664900"
        
//        let fcmToken = UserDefaults.standard.string(forKey: "FCMTOKEN")
//        self.roomUrl = NSString(format: "%@/r/%@-stdby", SERVER_HOST_URL, fcmToken!) as String
//        self.roomName = NSString(format: "%@-stdby", fcmToken!) as String
        
//        let email = UserDefaults.standard.string(forKey: "email")
//        self.roomUrl = NSString(format: "%@/r/%@-stdby", SERVER_HOST_URL, email!) as String
//        self.roomName = NSString(format: "%@-stdby", email!) as String
        
//        let email = UserDefaults.standard.string(forKey: "email")
//        self.roomUrl = NSString(format: "%@/%@-stdby", SERVER_HOST_URL, email!) as String
//        self.roomName = NSString(format: "%@-stdby", email!) as String
        
          let fcmToken = UserDefaults.standard.string(forKey: "FCMTOKEN")
          self.roomUrl = NSString(format: "%@/%@-stdby", SERVER_HOST_URL, fcmToken!) as String
          self.roomName = NSString(format: "%@-stdby", fcmToken!) as String
        
        NSLog("self.roomUrl :- %@",  self.roomUrl)
        NSLog("self.roomName :- %@", self.roomName)
        
        // Start Call Ringing
        //self.startRingin()
        self.connectToRoom()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.disconnect()
    }
    
    func roomNameAlertView() {
        
        let alert = UIAlertController(title: "Alert", message:"Please enter room id", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            switch action.style{
            case .default:
                print("default")
                
            case .cancel:
                print("cancel")
                
            case .destructive:
                print("destructive")
            }}))
        alert.addTextField(configurationHandler: {(textField : UITextField!) -> Void in
            textField.placeholder = "Room Id"
            textField?.delegate = self as? UITextFieldDelegate
        })
        self.present(alert, animated: true, completion: nil)
    }

    //MARK: AppRTC
    func connectToRoom()  {
        //Connect to the room
        self.disconnect()
        self.client = ARDAppClient.init(delegate: self)
        self.client.serverHostUrl = SERVER_HOST_URL
        //self.client.connectToRoom(withId: self.roomName, options: nil)
        
        let pubNubMessageResult = AppDelegate.getPubNubMessage()
        
        let messageDict = pubNubMessageResult.data.message as! NSDictionary
        print("Received message:\(messageDict.value(forKey: "call_user") as! String)")
        
        let pubNubClient = AppDelegate.getPubNubClient()
        //self.client.connectToChannel(withId: pubNubMessageResult.data.channel, callUser: (messageDict.value(forKey: "call_user") as! String))
        
//        self.client.connectToChannelWithPubNub(withId: pubNubMessageResult.data.channel, callUser: (messageDict.value(forKey: "call_user") as! String), pubNubClient: pubNubClient)
        
        self.client.connectToChannel(withPubNub: pubNubMessageResult.data.channel, callUser: (messageDict.value(forKey: "call_user") as! String), pubNubClient: pubNubClient)
    }

/*
    func connectToRoom()  {
        
        RTCPeerConnectionFactory.initialize()
        self.connectionFactory = RTCPeerConnectionFactory()
    }
*/
    
    func disconnect() -> Void {
//        if (self.client != nil) {
//            if self.localVideoTrack != nil{
//                self.localVideoTrack?.remove(self.localView)
//            }
//
//            if self.remoteVideoTrack != nil{
//                self.remoteVideoTrack?.remove(self.remoteView)
//            }
//
//            self.localView?.renderFrame(nil)
//            self.remoteView?.renderFrame(nil)
//            self.client?.disconnect()
//        }
    }
    
    func remoteDisconnected() -> Void {
        
        if self.remoteVideoTrack != nil{
            self.remoteVideoTrack?.remove(self.remoteView)
        }
        
        self.remoteView?.renderFrame(nil)
        self .videoView(self.localView, didChangeVideoSize: self.localVideoSize!)
    }
    
    //Mark: ARDAppClientDelegate
    func appClient(_ client: ARDAppClient!, didChange state: ARDAppClientState) {
        
        //        switch state {
        //        case kARDAppClientStateConnected :
        //                NSLog("Client connected.")
        //            break
        //        case kARDAppClientStateConnecting:
        //            NSLog("Client connecting.")
        //            break;
        //        case kARDAppClientStateDisconnected:
        //            NSLog("Client disconnected.")
        //            self.remoteDisconnected()
        //            break;
        //        default:
        //            break;
        //        }
        
    }
    
    func appClient(_ client: ARDAppClient!, didReceiveLocalVideoTrack localVideoTrack: RTCVideoTrack!) {
        
        if self.localVideoTrack != nil {
            self.localVideoTrack?.remove(self.localView)
            self.localView?.renderFrame(nil)
        }
        self.localVideoTrack = localVideoTrack
        self.localVideoTrack?.add(self.localView)
        
    }
    
    func appClient(_ client: ARDAppClient!, didReceiveRemoteVideoTrack remoteVideoTrack: RTCVideoTrack!) {
        
        self.remoteVideoTrack = remoteVideoTrack
        self.remoteVideoTrack?.add(self.remoteView)
        UIView.animate(withDuration: 0.4, animations: {
            
            //Instead of using 0.4 of screen size, we re-calculate the local view and keep our aspect ratio
            var orientation : UIDeviceOrientation!
            orientation = UIDevice.current.orientation
            
            var videoRect : CGRect
            videoRect = CGRect(x: 0.0, y: 0.0, width: self.view.frame.size.width/4.0, height: self.view.frame.size.height/4.0)
            
            if orientation == UIDeviceOrientation.landscapeLeft || orientation == UIDeviceOrientation.landscapeRight{
                videoRect = CGRect(x: 0.0, y: 0.0, width: self.view.frame.size.height/4.0, height: self.view.frame.size.width/4.0)
            }
        }, completion: { (finished: Bool) in
            
        })
        
    }
    
    func appClient(_ client: ARDAppClient!, didError error: Error!) {
        
        if (error != nil){
            let alert = UIAlertController(title: "Alert", message:NSString(format: "%@", error.localizedDescription) as String, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                switch action.style{
                case .default:
                    print("default")
                case .cancel:
                    print("cancel")
                case .destructive:
                    print("destructive")
                }}))
            self.present(alert, animated: true, completion: nil)
        }
        
        self.disconnect()
        
    }
    
    //Mark: - RTCEAGLVideoViewDelegate
    func videoView(_ videoView: RTCEAGLVideoView!, didChangeVideoSize size: CGSize) {
        
        //Resize the Local View depending if it is full screen or thumbnail
        self.localVideoSize = size
    }
    
    //MARK: Call Ringing Handler's
    func startRingin() -> Void {
        
//        let provider = CXProvider(configuration: CXProviderConfiguration(localizedName: "Skilleyes"))
//        provider.setDelegate(self, queue: nil)
//        let update = CXCallUpdate()
//        update.remoteHandle = CXHandle(type: .generic, value: "Skilleyes")
//        update.hasVideo = true
//        provider.reportNewIncomingCall(with: UUID(), update: update, completion: { error in })
        
                let registry = PKPushRegistry(queue: nil)
                registry.delegate = self
                registry.desiredPushTypes = [PKPushType.voIP]
    }
    
    func pushRegistry(_ registry: PKPushRegistry, didReceiveIncomingPushWith payload: PKPushPayload, for type: PKPushType, completion: @escaping () -> Void) {
        let config = CXProviderConfiguration(localizedName: "Skilleyes")
        config.iconTemplateImageData = UIImage(named: "skilleye")!.pngData()
        config.ringtoneSound = "ringtone.caf"
        if #available(iOS 11.0, *) {
            config.includesCallsInRecents = false
        } else {
            // Fallback on earlier versions
        }
        config.supportsVideo = true
        let provider = CXProvider(configuration: config)
        provider.setDelegate(self, queue: nil)
        let update = CXCallUpdate()
        update.remoteHandle = CXHandle(type: .generic, value: "Skilleyes")
        update.hasVideo = true
        provider.reportNewIncomingCall(with: UUID(), update: update, completion: { error in })
    }
    
    func providerDidReset(_ provider: CXProvider) {
    }
    
    func provider(_ provider: CXProvider, perform action: CXAnswerCallAction) {
        action.fulfill()
        
//        //Open Popup for Camera Choose
//        let cameraChooseView = cameraChoosePopup.init(frame: self.view.frame)
//        cameraChooseView.delegate = self
//        self.view.addSubview(cameraChooseView)
    }
    
    func provider(_ provider: CXProvider, perform action: CXEndCallAction) {
        action.fulfill()
        
        _ = navigationController?.popViewController(animated: true)
    }
    
    func pushRegistry(_ registry: PKPushRegistry, didUpdate pushCredentials: PKPushCredentials, for type: PKPushType) {
        print(pushCredentials.token.map { String(format: "%02.2hhx", $0) }.joined())
    }
    
    
    //Status Bar Color
    func setStatusBarBackgroundColor(color:UIColor)  {
        let statusBar = (UIApplication.shared.value(forKey: "statusBarWindow") as AnyObject).value(forKey: "statusBar") as? UIView
        statusBar?.backgroundColor = color
    }
    
    //If Camera is choosen
    func didTapCameraChoose(view: cameraChoosePopup, string: String) {
        //ViewController
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "VideoChatVC") as? VideoChatVC
        
        //Check Mode
        if(string == "skilleyes") {
            vc?.mode = "skilleyes"
        } else if(string == "handy") {
            vc?.mode = "handy"
        }
        
        //Call View
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    @IBAction func audioButtonPressed(_ sender: Any){
        //TODO: this change not work on simulator (it will crash)
        let audioButton = sender as! UIButton
        
        if (self.isAudioMute!) {
            self.client.unmuteAudioIn()
            audioButton.setImage(UIImage(named: "audioOn"), for: .normal)
            self.isAudioMute = false;
        } else {
            self.client.muteAudioIn()
            audioButton.setImage(UIImage(named: "audioOff"), for: .normal)
            self.isAudioMute = true;
        }
    }

    @IBAction func videoButtonPressed(_ sender: Any){
        //TODO: this change not work on simulator (it will crash)
        let videoButton = sender as! UIButton
        if (self.isVideoMute!) {
            self.client.swapCameraToFront()
            videoButton.setImage(UIImage(named: "videoOn"), for: .normal)
            self.isVideoMute = false
        }else{
            self.client.swapCameraToBack()
            self.isVideoMute = true
        }
        
    }
    
    @IBAction func hangupButtonPressed(_ sender: Any){
        //Clean up
        self.disconnect()
        self.navigationController?.popViewController(animated: true)
    }
    
}

/*
 extension CGRect {
 
 func scaleToAspectFit(in rtarget: CGRect) -> CGFloat {
 // first try to match width
 let s = rtarget.width / self.width;
 // if we scale the height to make the widths equal, does it still fit?
 if self.height * s <= rtarget.height {
 return s
 }
 // no, match height instead
 return rtarget.height / self.height
 }
 
 func aspectFit(in rtarget: CGRect) -> CGRect {
 let s = scaleToAspectFit(in: rtarget)
 let w = width * s
 let h = height * s
 let x = rtarget.midX - w / 2
 let y = rtarget.midY - h / 2
 return CGRect(x: x, y: y, width: w, height: h)
 }
 
 func scaleToAspectFit(around rtarget: CGRect) -> CGFloat {
 // fit in the target inside the rectangle instead, and take the reciprocal
 return 1 / rtarget.scaleToAspectFit(in: self)
 }
 
 func aspectFit(around rtarget: CGRect) -> CGRect {
 let s = scaleToAspectFit(around: rtarget)
 let w = width * s
 let h = height * s
 let x = rtarget.midX - w / 2
 let y = rtarget.midY - h / 2
 return CGRect(x: x, y: y, width: w, height: h)
 }
 }
 */
