//
//  cameraChoosePopup.swift
//  Skilleyes
//
//  Created by Dirk Reiher on 09.01.19.
//  Copyright © 2019 Mac HD. All rights reserved.
//

import UIKit

//Typealias
typealias MethodHandlerRythm = ()  -> Void

//Protocol
protocol CameraChooseDelegate:class {
    func didTapCameraChoose(view:cameraChoosePopup, string:String)
}

//Popup Class
class cameraChoosePopup: UIView {
    
    //Variable
    weak var delegate :CameraChooseDelegate?
    
    //Outlets
    @IBOutlet var bgView: UIView!
    @IBOutlet var view: UIView!
    
    //Init
    override init(frame:CGRect){
        super.init(frame: frame)
        
        setUpScreen()
    }
    
    //Required Init
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    //Skilleyes Mode
    @IBAction func skilleyesBtnTapped(_ sender: UIButton) {
        self.removeFromSuperview()
        self.delegate?.didTapCameraChoose(view: self, string: "skilleyes")
    }
    
    //Handy Mode
    @IBAction func HandybtnTapped(_ sender: UIButton) {
        self.removeFromSuperview()
        self.delegate?.didTapCameraChoose(view: self, string: "handy")
    }
    
    //Setup Screen
    func setUpScreen(){
        self.view = Bundle.main.loadNibNamed("cameraChoosePopup", owner: self, options: nil)?[0] as? UIView
        
        self.bgView.layer.borderWidth = 0
        self.bgView.layer.borderColor = UIColor.white.cgColor
        self.view?.frame = self.bounds
        self.addSubview(self.view!)
    }
}

