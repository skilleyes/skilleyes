//
//  LayerTableVC.swift
//  Measure Buddy
//
//  Created by WebvilleeMAC on 15/02/17.
//  Copyright © 2017 Ishan Gupta. All rights reserved.
//

import UIKit

class LayerTableVC: UITableViewController {
    
    var selectedLocation = ""
    var from = ""

    var listArray = NSArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = #colorLiteral(red: 0.9529411765, green: 0.9568627451, blue: 0.9607843137, alpha: 1)
        self.tableView.separatorStyle = UITableViewCell.SeparatorStyle.none
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return listArray.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       let cell = UITableViewCell.init()
        let jsonDict = listArray[indexPath.row] as! NSDictionary
        print(jsonDict)
       
        if from == "langFilter" {
            
            if selectedLocation == jsonDict.object(forKey: "lang") as! String
            {
                cell.accessoryType =  .checkmark
            }
            else
            {
                cell.accessoryType =  .none
            }
            cell.textLabel?.text =  jsonDict.object(forKey: "lang") as? String

        }
       else if from == "categryFilter" {

            if selectedLocation == jsonDict.object(forKey: "name") as! String
            {
                cell.accessoryType =  .checkmark
            }
            else
            {
                cell.accessoryType =  .none
            }
            cell.textLabel?.text =  jsonDict.object(forKey: "name") as? String

        }
        else if from == "typeFilter" {
            if selectedLocation == jsonDict.object(forKey: "name") as! String
            {
                cell.accessoryType =  .checkmark
            }
            else
            {
                cell.accessoryType =  .none
            }
            cell.textLabel?.text =  jsonDict.object(forKey: "name") as? String
        }
        else if from == "curencyFilter" {

            if selectedLocation == String.init(format: "%@ %@", jsonDict.object(forKey: "currency_symbol") as! CVarArg , jsonDict.object(forKey: "currency_code") as! CVarArg)
               // jsonDict.object(forKey: "currency_code") as! String
            {
                cell.accessoryType =  .checkmark
            }
            else
            {
                cell.accessoryType =  .none
            }
            cell.textLabel?.text = String.init(format: "%@ %@", jsonDict.object(forKey: "currency_symbol") as! CVarArg , jsonDict.object(forKey: "currency_code") as! CVarArg)
//                jsonDict.object(forKey: "currency_code") as? String
//
        }else if from == "curencyTMFilter" {
            
            if selectedLocation == String.init(format: "%@ %@", jsonDict.object(forKey: "currency_symbol") as! CVarArg , jsonDict.object(forKey: "currency_code") as! CVarArg)
                // jsonDict.object(forKey: "currency_code") as! String
            {
                cell.accessoryType =  .checkmark
            }
            else
            {
                cell.accessoryType =  .none
            }
            cell.textLabel?.text = String.init(format: "%@ %@", jsonDict.object(forKey: "currency_symbol") as! CVarArg , jsonDict.object(forKey: "currency_code") as! CVarArg)
            //                jsonDict.object(forKey: "currency_code") as? String
            //curencyTMFilter
        }
        else if from == "categry2Filter" {

            if selectedLocation == jsonDict.object(forKey: "name") as! String
            {
                cell.accessoryType =  .checkmark
            }
            else
            {
                cell.accessoryType =  .none
            }
            cell.textLabel?.text =  jsonDict.object(forKey: "name") as? String

        }
        else if from == "categry3Filter" {
            if selectedLocation == jsonDict.object(forKey: "name") as! String
            {
                cell.accessoryType =  .checkmark
            }
            else
            {
                cell.accessoryType =  .none
            }
            cell.textLabel?.text =  jsonDict.object(forKey: "name") as? String
        }
        else if from == "categry4Filter" {
            if selectedLocation == jsonDict.object(forKey: "name") as! String
            {
                cell.accessoryType =  .checkmark
            }
            else
            {
                cell.accessoryType =  .none
            }
            cell.textLabel?.text =  jsonDict.object(forKey: "name") as? String
        }
            else if from == "Vew1Filter" {
            if selectedLocation == jsonDict.object(forKey: "name") as! String
            {
                cell.accessoryType =  .checkmark
            }
            else
            {
                cell.accessoryType =  .none
            }
            cell.textLabel?.text =  jsonDict.object(forKey: "name") as? String
        }
        else if from == "group" {
            if selectedLocation == jsonDict.object(forKey: "groupname") as! String
            {
                cell.accessoryType =  .checkmark
            }
            else
            {
                cell.accessoryType =  .none
            }
            cell.textLabel?.text =  jsonDict.object(forKey: "groupname") as? String
        }else if from == "group2" {
            if selectedLocation == jsonDict.object(forKey: "groupname") as! String
            {
                cell.accessoryType =  .checkmark
            }
            else
            {
                cell.accessoryType =  .none
            }
            cell.textLabel?.text =  jsonDict.object(forKey: "groupname") as? String
        }//
        else if from == "locationFilter" {
            if selectedLocation == jsonDict.object(forKey: "Dist") as! String
            {
                cell.accessoryType =  .checkmark
            }
            else
            {
                cell.accessoryType =  .none
            }
            cell.textLabel?.text =  jsonDict.object(forKey: "Dist") as? String
        }
        
        // Configure the cell...
      cell.backgroundColor = #colorLiteral(red: 0.9529411765, green: 0.9568627451, blue: 0.9607843137, alpha: 1)
      cell.textLabel?.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
      cell.selectionStyle = .none
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let jsonDict = listArray[indexPath.row] as! NSDictionary
        
        if from == "" {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: Singleton.shared.LOCATION_SELECTED), object: jsonDict)
        }else{
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: Singleton.shared.LOCATION_SELECTED), object: [jsonDict,from])
            self.dismiss(animated: false, completion: nil)
        }
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
}
