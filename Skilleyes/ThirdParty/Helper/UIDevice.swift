//
//  UIDevice.swift
//  Skilleyes
//
//  Created by Mac HD on 04/12/18.
//  Copyright © 2018 Mac HD. All rights reserved.
//

import Foundation

public extension UIDevice {

    var modelName: String {
        #if targetEnvironment(simulator)
        let DEVICE_IS_SIMULATOR = true
        #else
        let DEVICE_IS_SIMULATOR = false
        #endif

        var machineString : String = ""

        if DEVICE_IS_SIMULATOR == true
        {

//            if let dir = ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] {
//                machineString = dir
//            }
        }
        else {
            var systemInfo = utsname()
            uname(&systemInfo)
            let machineMirror = Mirror(reflecting: systemInfo.machine)
            machineString = machineMirror.children.reduce("") { identifier, element in
                guard let value = element.value as? Int8, value != 0 else { return identifier }
                return identifier + String(UnicodeScalar(UInt8(value)))
            }
        }
        switch machineString {
            
        case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return "iPhone4"
        case "iPhone4,1", "iPhone4,2", "iPhone4,3":     return "iPhone4S"
        case "iPhone5,1", "iPhone5,2":                 return "iPhone5"
        case "iPhone5,3", "iPhone5,4":                 return "iPhone5C"
        case "iPhone6,1", "iPhone6,2":                 return "iPhone5S"
        case "iPhone7,2":                            return "iPhone6"
        case "iPhone7,1":                            return "iPhone6Plus"
        case "iPhone8,1":                            return "iPhone6S"
        case "iPhone8,2":                            return "iPhone6SPlus"
        case "iPhone8,3", "iPhone8,4":                 return "iPhoneSE"
        case "iPhone9,1", "iPhone9,3":                 return "iPhone7"
        case "iPhone9,2", "iPhone9,4":                 return "iPhone7Plus"
        case "iPhone10,1", "iPhone10,4":               return "iPhone8"
        case "iPhone10,2", "iPhone10,5":               return "iPhone8Plus"
        case "iPhone10,3", "iPhone10,6":               return "iPhoneX"
        case "iPhone11,2":                           return "iPhoneXS"
        case "iPhone11,4", "iPhone11,6":               return "iPhoneXS_Max"
        case "iPhone11,8":                           return "iPhoneXR"
            
            /*** iPad ***/
        case "iPad1,1", "iPad1,2":                    return "Version.iPad1"
        case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4": return "Version.iPad2"
        case "iPad3,1", "iPad3,2", "iPad3,3":           return "Version.iPad3"
        case "iPad3,4", "iPad3,5", "iPad3,6":           return "Version.iPad4"
        case "iPad6,11", "iPad6,12":                   return "Version.iPad5"
        case "iPad7,5", "iPad 7,6":                    return "Version.iPad6"
        case "iPad4,1", "iPad4,2", "iPad4,3":           return "Version.iPadAir"
        case "iPad5,3", "iPad5,4":                     return "Version.iPadAir2"
        case "iPad2,5", "iPad2,6", "iPad2,7":           return "Version.iPadMini"
        case "iPad4,4", "iPad4,5", "iPad4,6":           return "Version.iPadMini2"
        case "iPad4,7", "iPad4,8", "iPad4,9":           return "Version.iPadMini3"
        case "iPad5,1", "iPad5,2":                     return "Version.iPadMini4"
        case "iPad6,7", "iPad6,8", "iPad7,1", "iPad7,2":  return "Version.iPadPro12_9Inch"
        case "iPad7,3", "iPad7,4":                       return "Version.iPadPro10_5Inch"
        case "iPad6,3", "iPad6,4":                       return "Version.iPadPro9_7Inch"
            
            /*** iPod ***/
        case "iPod1,1":                                  return "iPodTouch1Gen"
        case "iPod2,1":                                  return "iPodTouch2Gen"
        case "iPod3,1":                                  return "iPodTouch3Gen"
        case "iPod4,1":                                  return "iPodTouch4Gen"
        case "iPod5,1":                                  return "iPodTouch5Gen"
        case "iPod7,1":                                  return "iPodTouch6Gen"
//
//            /*** Simulator ***/
        case "i386", "x86_64":                           return "simulator"
            
        default:                                        return machineString
        }
    }
}


