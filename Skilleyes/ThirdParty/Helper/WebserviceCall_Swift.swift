//
//  WebserviceCall_Swift.swift
//  RURProject
//
//  Created by WebvilleeMAC on 23/08/17.
//  Copyright © 2017 Webvillee. All rights reserved.
//

import UIKit

class WebserviceCall_Swift: NSObject,URLSessionTaskDelegate,URLSessionDataDelegate {
    var delegatee:AnyObject?
    var callBackk:Selector?
    var session:URLSession?
    var failedSession:URLSession?
    var recieveData = Data()
    var aces_token : String!
    var appDel = UIApplication.shared.delegate as! AppDelegate
    static let shared = WebserviceCall_Swift()
    let KEY_GOOGLE = " AIzaSyCuO_c7uc091L6fL5mV9jPxezhCZdOOZdE "
    
    let boundary = "Boundary-\(UUID().uuidString)"
    
    //static var server_url = "https://skill-eyes.com/api/"
    //static var server_url = "https://skill-eyes.com/api/"
    static var server_url =  "https://dev.ios.skilleyes.com/api/"
//    static var image_url = "https://augahi.com/images/ads/"
     //static var server_url =  "https://api.core.skilleyes.com/api/";
    
//    static var server_url = "https://webandappdevelopers.com/augahi/api/user/"
//    static var image_url = "https://webandappdevelopers.com/augahi/api/user/"
    
    func initWithDelegate(delegate:AnyObject , callBack:Selector) -> Any
    {
        
        delegatee = delegate
        callBackk = callBack
        
        return self
    }
    
    func retryService()
    {
        _ = failedSession
    }
    
    func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive data: Data)
    {
        print(data)
        //  print(recieveData)
        
        // recieveData.append(data)
    }
    
    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?)
    {
        if (error != nil)
        {
            failedSession = session
            _ =  delegatee?.perform(callBackk, with: "error")
            //print("didCompleteWithError \(error)")
        }else
        {
            //            do {
            //                _ = try JSONSerialization.jsonObject(with: recieveData, options: JSONSerialization.ReadingOptions.allowFragments)
            //
            //                print(json)
            //                recieveData = Data()
            //            } catch  {
            //                _ =  delegatee?.perform(callBackk, with: "error")
            //                recieveData = Data()
            //                print("error")
            //            }
        }
    }
    
    func serviceCall1(parameterDict:Dictionary<String, Any> , url:String )
    {
        let url = URL(string: WebserviceCall_Swift.server_url + url)!
        print(url)
        var req = URLRequest.init(url: url)
        req.httpMethod = "POST"
        req.timeoutInterval = 10
        
        //req.setValue(postLength, forHTTPHeaderField: "Content-Length")
        req.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        req.setValue("application/json", forHTTPHeaderField: "Accept")
        req.setBodyContent(parameterDict as Dictionary<String, Any> as! [String : String])
        
        let sessionConf = URLSessionConfiguration.default
        session = URLSession.init(configuration: sessionConf)
        let dataTask = session?.dataTask(with: req as URLRequest) {data,response,error in
            let httpResponse = response as? HTTPURLResponse
            var lastPath = ""
            
            if httpResponse != nil
            {
                lastPath =  (httpResponse?.url?.lastPathComponent)!
            }
            
            if (error != nil)
            {
                print(error!)
                _ =  self.delegatee?.perform(self.callBackk, with: "error")
                
            }
            else
            {
                do
                {
                    let json = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                    print("resulted json", json)
                    
                    _ =  self.delegatee?.perform(self.callBackk, with: [json,lastPath] as NSArray)
                }
                catch
                {
                    print("json error: \(error)")
                    _ =  self.delegatee?.perform(self.callBackk, with: "error")
                }
            }
            
            DispatchQueue.main.async
                {
                    //Update your UI here
            }
        }
        dataTask?.resume()
    }
    
    func serviceCall(parameterDict:Dictionary<String, String> , url:String )
    {
        let url = URL(string: WebserviceCall_Swift.server_url + url)!
        print(url)
        var req = URLRequest.init(url: url)
        req.httpMethod = "POST"
        req.timeoutInterval = 10
        
        if (UserDefaults.standard.string(forKey: "login_accesstoken") != nil)
        {
            self.aces_token = UserDefaults.standard.value(forKey: "login_accesstoken") as? String
            print(self.aces_token)
        }else {
            self.aces_token = ""
        }
//        self.aces_token = String.init(format: "%@", dictUserInfo?.object(forKey: "access_token") as! CVarArg)
//        print(self.aces_token!)
        
        //req.setValue(postLength, forHTTPHeaderField: "Content-Length")
        req.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        req.setValue("application/json", forHTTPHeaderField: "Accept")
        req.setValue("Bearer \(self.aces_token!)", forHTTPHeaderField: "Authorization")
        print(req)
        req.setBodyContent(parameterDict)
        
        let sessionConf = URLSessionConfiguration.default
        session = URLSession.init(configuration: sessionConf)
        let dataTask = session?.dataTask(with: req as URLRequest) {data,response,error in
            let httpResponse = response as? HTTPURLResponse
            var lastPath = ""
          
            if httpResponse != nil
            {
                lastPath =  (httpResponse?.url?.lastPathComponent)!
            }
            
            if (error != nil)
            {
                print(error!)
                _ =  self.delegatee?.perform(self.callBackk, with: "error")
                
            }
            else
            {
                do
                {
                    let json = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                    print("resulted json", json)
                    
                    _ =  self.delegatee?.perform(self.callBackk, with: [json,lastPath] as NSArray)
                }
                catch
                {
                    print("json error: \(error)")
                    _ =  self.delegatee?.perform(self.callBackk, with: "error")
                }
            }
            
            DispatchQueue.main.async
                {
                //Update your UI here
            }
        }
        dataTask?.resume()
    }
    
    func serviceCallWith(image:UIImage,url:String ,parameterDict:Dictionary<String, String>,image_upload_key:String) {
        
        let url = URL(string: WebserviceCall_Swift.server_url + url)!
        print(url)
        var req = URLRequest.init(url: url)
        req.httpMethod = "POST"
        req.timeoutInterval = 10
        
        req.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")

        let body = createBody(parameters: parameterDict, boundary: boundary, data: image.jpegData(compressionQuality: 0.8)!, mimeType: "image/jpg",
                              filename: "business.jpg",img_key:image_upload_key)
        
//        UIImageJPEGRepresentation(image, 0.8)
        print(body)
        req.httpBody =   body
        
        let postLength = "\(body.count)"
        req.setValue(postLength, forHTTPHeaderField: "Content-Length")
        let sessionConf = URLSessionConfiguration.default
        session = URLSession.init(configuration: sessionConf)
        let dataTask = session?.dataTask(with: req as URLRequest) {data,response,error in
            let httpResponse = response as? HTTPURLResponse
            var lastPath = ""
            if httpResponse != nil{
                lastPath =  (httpResponse?.url?.lastPathComponent)!
            }
            
            if (error != nil) {
                print(error!)
                _ =  self.delegatee?.perform(self.callBackk, with: "error")
                
            } else {
                //print(httpResponse!)
                do {
                    let json = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                    print(json)
                    
                    _ =  self.delegatee?.perform(self.callBackk, with: [json,lastPath] as NSArray)
                    
                } catch {
                    print("json error: \(error)")
                    _ =  self.delegatee?.perform(self.callBackk, with: "error")
                }
            }
            DispatchQueue.main.async {
                //Update your UI here
            }
        }
        dataTask?.resume()
    }
    
    func createBody(parameters: [String: String],
                    boundary: String,
                    data: Data,
                    mimeType: String,
                    filename: String,img_key:String) -> Data {
        let body = NSMutableData()
        
        let boundaryPrefix = "--\(boundary)\r\n"
        
        for (key, value) in parameters {
            body.appendString(boundaryPrefix)
            body.appendString("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
            body.appendString("\(value)\r\n")
        }
        
        body.appendString(boundaryPrefix)
        body.appendString("Content-Disposition: form-data; name=\"\(img_key)\"; filename=\"\(filename)\"\r\n") // profile_pic is KEY to upload Image
        body.appendString("Content-Type: \(mimeType)\r\n\r\n")
        body.append(data)
        body.appendString("\r\n")
        body.appendString("--".appending(boundary.appending("--")))
        
        return body as Data
    }
    
    
    func generateBoundaryString() -> String {
        return "0xKhTmLbOuNdArY"//"Boundary-\(NSUUID().uuidString)"
    }
    
    func googleServiceCalling(with geoID:String , isState:String) {
        let url = URL(string: "http://api.geonames.org/childrenJSON?geonameId=\(geoID)&username=webvilleedeveloper")!
        print(url)
        var req = URLRequest.init(url: url)
        req.httpMethod = "POST"
        req.timeoutInterval = 60
        
        //req.setValue(postLength, forHTTPHeaderField: "Content-Length")
        req.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        req.setValue("application/json", forHTTPHeaderField: "Accept")
        
        let sessionConf = URLSessionConfiguration.default
        session = URLSession.init(configuration: sessionConf)
        let dataTask = session?.dataTask(with: req as URLRequest) {data,response,error in
            let httpResponse = response as? HTTPURLResponse
            var lastPath = ""
            
            if httpResponse != nil
            {
                lastPath =  (httpResponse?.url?.lastPathComponent)!
            }
            
            if (error != nil)
            {
                print(error!)
                _ =  self.delegatee?.perform(self.callBackk, with: "error")
                
            }
            else
            {
                //print(httpResponse!)
                do
                {
                    let json = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                    print("resulted json", json)
                    
                    _ =  self.delegatee?.perform(self.callBackk, with: [json,lastPath,isState] as NSArray)
                }
                catch
                {
                    print("json error: \(error)")
                    _ =  self.delegatee?.perform(self.callBackk, with: "error")
                }
            }
            
            DispatchQueue.main.async
                {
                    //Update your UI here
            }
        }
        
        dataTask?.resume()
    }
    func getCountry(){
        
        let url = URL(string: "http://api.geonames.org/countryInfoJSON?username=webvilleedeveloper")
        var req = URLRequest.init(url: url!)
        req.httpMethod = "POST"
        req.timeoutInterval = 60
        
        //req.setValue(postLength, forHTTPHeaderField: "Content-Length")
        req.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        req.setValue("application/json", forHTTPHeaderField: "Accept")
        
        let sessionConf = URLSessionConfiguration.default
        session = URLSession.init(configuration: sessionConf)
        let dataTask = session?.dataTask(with: req as URLRequest) {data,response,error in
            let httpResponse = response as? HTTPURLResponse
            var lastPath = ""
            
            if httpResponse != nil
            {
                lastPath =  (httpResponse?.url?.lastPathComponent)!
            }
            
            if (error != nil)
            {
                print(error!)
                _ =  self.delegatee?.perform(self.callBackk, with: "error")
                
            }
            else
            {
                //print(httpResponse!)
                do
                {
                    let json = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                    print("resulted json", json)
                    
                    _ =  self.delegatee?.perform(self.callBackk, with: [json,lastPath] as NSArray)
                }
                catch
                {
                    print("json error: \(error)")
                    _ =  self.delegatee?.perform(self.callBackk, with: "error")
                }
            }
            
            DispatchQueue.main.async
                {
                    //Update your UI here
            }
        }
        
        dataTask?.resume()
        
    }
    
    func googleAPICall(placeName:String) {
        var components = URLComponents(string: "https://maps.googleapis.com/maps/api/geocode/json")!
        let key = URLQueryItem(name: "key", value: KEY_GOOGLE) // use your key
        let address = URLQueryItem(name: "address", value: placeName)
        components.queryItems = [key, address]
        
        let task = URLSession.shared.dataTask(with: components.url!) { data, response, error in
            guard let data = data, let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode == 200, error == nil else {
                //                print(String(describing: response))
                //                print(String(describing: error))
                return
            }
            
            guard let json = try! JSONSerialization.jsonObject(with: data) as? [String: Any] else {
                //                print("not JSON format expected")
                //                print(String(data: data, encoding: .utf8) ?? "Not string?!?")
                return
            }
            
            guard let results = json["results"] as? [[String: Any]],
                let status = json["status"] as? String,
                status == "OK" else {
                    print("no results")
                    print(String(describing: json))
                    return
            }
            
            DispatchQueue.main.async {
                // now do something with the results, e.g. grab `formatted_address`:
                
                
                let arrResult = results as NSArray
                let resultDict = arrResult.object(at: 0) as! NSDictionary
                if resultDict.object(forKey: "geometry") != nil {
                let geoDict = resultDict.object(forKey: "geometry") as! NSDictionary
                    if geoDict.object(forKey: "location") != nil {
                    let boundDict = geoDict.object(forKey: "location") as! NSDictionary
                        if boundDict.count > 0 {
                                        _ =  self.delegatee?.perform(self.callBackk, with: [boundDict,"geolocation"] as NSArray)
                    }
                }
                    
                }
//                 _ =  self.delegatee?.perform(self.callBackk, with: [json,lastPath] as NSArray)
                
            }
        }
        
        task.resume()
        
    }


}
extension NSMutableData {
    func appendString(_ string: String) {
        let data = string.data(using: String.Encoding.utf8, allowLossyConversion: false)
        append(data!)
    }
}
