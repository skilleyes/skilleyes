//
//  Singleton.swift
//  RURProject
//
//  Created by WebvilleeMAC on 25/08/17.
//  Copyright © 2017 Webvillee. All rights reserved.
//

import UIKit

class Singleton: NSObject {
    
    static let shared  = Singleton()
    let SOCKET_NOTIFICATION_NAME = "socket_notification"
    let HINT_POINT = "hint_point"
    let RIGHT_ANSWER_POINT = "right_answer_point"
    var USERID = ""
    var ORDERID = ""
    var ROUNDUP = ""
    var error = ""
    var BUSINESSID = ""
    var USER_CURRENT_LAT:Double = 0.0
    var USER_CURRENT_LNG:Double = 0.0
    let LOCATION_SELECTED = "locationSelectedNotification"
    var STATENAME = ""
    var CELLTOTAL = ""
    
    let STARTGAME_NOTIFICATION = "startgame_notification"
    let SAVEANSWER_NOTIFICATION = "saveanswer_notification"
    let FINALRESULT_NOTIFICATION_NAME = "final_result_notification"
    let OPPONENTLEFT_NOTIFICATION_NAME = "opponent_left_notification"
    var isGameJoined = false
    var isConsumer = true
    var arrCountry = Array<Any>()
    var arrCategory = NSArray()

    func getCurrentUnixTime() -> String {
        let unixTime = String.init(format: "%f", NSDate().timeIntervalSince1970)
        let splitArray = unixTime.components(separatedBy: ".")
        print("Current unix timestamp \(splitArray[0])")
        return splitArray[0]
    }
    
    
    func convertDictionaryToJsonString(dict:NSDictionary) -> String {
        
        var jsonString = ""
        do {
            let data = try JSONSerialization.data(withJSONObject: dict, options: JSONSerialization.WritingOptions.prettyPrinted)
            jsonString = String.init(data: data, encoding: .utf8)!
        } catch {
            print("Exception in convertDictToJSONString")
        }
        return jsonString
        
    }
    
    func convertDictionaryToSocketData(dict:Dictionary<AnyHashable, Any>,eventName:String) {
        
        let jsonStr = self.convertDictionaryToJsonString(dict: dict as NSDictionary)
        let arrEvent = [eventName,jsonStr] as NSArray
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: Singleton.shared.SOCKET_NOTIFICATION_NAME), object: nil, userInfo: ["event":arrEvent])
        
    }
    
    func roundCornerButton(btn:UIButton,corner_rad:CGFloat)
    {
        btn.layer.cornerRadius = corner_rad
        btn.layer.shadowColor = btn.backgroundColor?.cgColor
        btn.layer.shadowRadius = 1
        btn.layer.shadowOffset = CGSize.init(width: 2, height: 5)
        btn.layer.shadowOpacity = 0.2
    }
    
    func addShadowInView(view:UIView , borderColor:UIColor , borderWidth : CGFloat , shadowColor:UIColor ,shadowRadius : CGFloat)  {
        view.layer.borderColor = borderColor.cgColor
        view.layer.borderWidth = borderWidth
        view.layer.shadowColor = shadowColor.cgColor
        view.layer.shadowRadius = shadowRadius
        view.layer.shadowOffset = CGSize.init(width: 3, height: 5)
        view.layer.shadowOpacity = 0.1
    }
    
    func convertTimeStampToDate(timeStamp:String , withTime:Bool , dateFormat:String , timeFormat:String) -> String {
        
        let timeSta = Double(timeStamp)
        let interval = TimeInterval.init(timeSta!)
        let date = Date.init(timeIntervalSince1970: interval)
        let formatter = DateFormatter.init()
        formatter.locale = Locale.current
        if !withTime {
            formatter.dateFormat = dateFormat
            
        }else{
            formatter.dateFormat = "\(dateFormat) \(timeFormat)"
            
        }
        return formatter.string(from: date);
        
    }
    func formatPhoneNumber(_ simpleNum: String, deleteLastChar: Bool) -> String {
        var simpleNumber = simpleNum
        if (simpleNumber.characters.count ) == 0 {
            return ""
        }
        // use regex to remove non-digits(including spaces) so we are left with just the numbers
        let regex = try? NSRegularExpression(pattern: "[\\s-\\(\\)]", options: .caseInsensitive)
        simpleNumber = (regex?.stringByReplacingMatches(in: simpleNumber, options: [], range: NSRange(location: 0, length: (simpleNumber.characters.count )), withTemplate: ""))!
        // check if the number is to long
        
        if (simpleNumber.characters.count ) > 10 {
            // remove last extra chars.
            //simpleNumber = ((simpleNumber as? NSString)?.substring(from: simpleNumber.length - 1))!
            let length = Int(simpleNumber.characters.count )
            if length > 10 {
                simpleNumber = ((simpleNumber as? NSString)?.substring(from: length - 10))!
            }
            
        }
        if deleteLastChar {
            // should we delete the last digit?
            simpleNumber = ((simpleNumber as? NSString)?.substring(to: (simpleNumber.characters.count ) - 1))!
            
        }
        if simpleNumber.length < 7 {
            simpleNumber = (simpleNumber as NSString).replacingOccurrences(of: "(\\d{3})(\\d+)", with: "$1-$2", options: .regularExpression, range: NSRange(location: 0, length: simpleNumber.length))
        }
        else {
            // else do this one..
            simpleNumber = (simpleNumber as NSString).replacingOccurrences(of: "(\\d{3})(\\d{3})(\\d+)", with: "$1-$2-$3", options: .regularExpression, range: NSRange(location: 0, length: simpleNumber.length))
        }
        return simpleNumber
    }
    
    func getCategoryNameFromID(id:String) -> String  {
        
        for item in arrCategory {
            if String.init(format: "%@", (item as! NSDictionary).object(forKey: "id") as! CVarArg) == id {
                return String.init(format: "%@", (item as! NSDictionary).object(forKey: "name") as! CVarArg)
            }
        }
        return ""
    }
    class func convertToString(convert:Any) -> String {
        
        return String.init(format: "%@", convert as! CVarArg)
        
    }
    
    class func convertTimeFormatTo12Hour(time:String) -> String {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm:ss"
        let date = formatter.date(from: time)
        formatter.dateFormat = "hh:mm a"
        return formatter.string(from: date!)
        
    }
    
    class func convertDateFormat(date:String,format:String,oldFormat:String) -> String {
        
        let formatter = DateFormatter()
        formatter.dateFormat = oldFormat
        formatter.locale = .current
        let date1 = formatter.date(from: date)
        formatter.dateFormat = format
        return formatter.string(from: date1!)
        
    }
    
    
}

@IBDesignable
class RightAlignedIconButton: UIButton {
    override func layoutSubviews() {
        super.layoutSubviews()
        if #available(iOS 9.0, *) {
            semanticContentAttribute = .forceRightToLeft
        } else {
            // Fallback on earlier versions
        }
        contentHorizontalAlignment = .right
        let availableSpace = bounds.inset(by: contentEdgeInsets)

//            UIEdgeInsetsInsetRect(bounds, contentEdgeInsets)
        let availableWidth = availableSpace.width - imageEdgeInsets.left - (imageView?.frame.width ?? 0) - (titleLabel?.frame.width ?? 0)
        titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: availableWidth / 2)
    }
}


