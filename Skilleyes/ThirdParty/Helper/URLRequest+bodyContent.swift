//
//  URLRequest+bodyContent.swift
//  RURProject
//
//  Created by WebvilleeMAC on 25/08/17.
//  Copyright © 2017 Webvillee. All rights reserved.
//

import Foundation
extension URLRequest {
    
    /// Populate the HTTPBody of `application/x-www-form-urlencoded` request
    ///
    /// - parameter parameters:   A dictionary of keys and values to be added to the request
    
    mutating func setBodyContent(_ parameters: [String : String]) {
        let parameterArray = parameters.map { (key, value) -> String in
            return "\(key.addingPercentEncodingForQuery()!)=\(value.addingPercentEncodingForQuery()!)"
        }
        httpBody = parameterArray.joined(separator: "&").data(using: .utf8)
    }
    
//    mutating func setBodyContent1(_ parameters: [String : Any]) {
//        let parameterArray = parameters.map { (arg) -> String in
//
//            let (key, value) = arg
//            return "\(key.addingPercentEncodingForQuery()!)=\(value.addingPercentEncodingForQuery()!)"
//        }
//        httpBody = parameterArray.joined(separator: "&").data(using: .utf8)
//    }
}
