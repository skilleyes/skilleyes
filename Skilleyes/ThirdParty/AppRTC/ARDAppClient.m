/*
 * libjingle
 * Copyright 2014, Google Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  1. Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *  2. Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *  3. The name of the author may not be used to endorse or promote products
 *     derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 * EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#import "ARDAppClient.h"

#import <AVFoundation/AVFoundation.h>

#import "ARDMessageResponse.h"
#import "ARDRegisterResponse.h"
#import "ARDSignalingMessage.h"
#import "ARDUtilities.h"
#import "ARDWebSocketChannel.h"
#import "RTCICECandidate+JSON.h"
#import "RTCICEServer+JSON.h"
#import "RTCMediaConstraints.h"
#import "RTCMediaStream.h"
#import "RTCPair.h"
#import "RTCPeerConnection.h"
#import "RTCPeerConnectionDelegate.h"
#import "RTCPeerConnectionFactory.h"
#import "RTCSessionDescription+JSON.h"
#import "RTCSessionDescriptionDelegate.h"
#import "RTCVideoCapturer.h"
#import "RTCVideoTrack.h"

static NSString *kDefaultSTUNServerUrl = @"stun:e5.xirsys.com";
static NSString *kDefaultSTUNServerUsername = @"603febb8-5e6e-11e8-8742-3a11f5d3b782";
static NSString *kDefaultSTUNServerPassword = @"603fec26-5e6e-11e8-ab1a-a6a06791d4fd";

static NSString *kARDAppClientErrorDomain = @"SkillEyes";
static NSInteger kARDAppClientErrorUnknown = -1;
static NSInteger kARDAppClientErrorRoomFull = -2;
static NSInteger kARDAppClientErrorCreateSDP = -3;
static NSInteger kARDAppClientErrorSetSDP = -4;
static NSInteger kARDAppClientErrorNetwork = -5;
static NSInteger kARDAppClientErrorInvalidClient = -6;
static NSInteger kARDAppClientErrorInvalidRoom = -7;

@interface ARDAppClient () <RTCPeerConnectionDelegate, RTCSessionDescriptionDelegate>

@property(nonatomic, strong) RTCPeerConnection *peerConnection;
@property(nonatomic, strong) RTCPeerConnectionFactory *factory;
@property(nonatomic, strong) NSMutableArray *messageQueue;

@property(nonatomic, assign) BOOL isTurnComplete;
@property(nonatomic, assign) BOOL hasReceivedSdp;
@property(nonatomic, readonly) BOOL isRegisteredWithRoomServer;

@property(nonatomic, strong) NSString *roomId;
@property(nonatomic, strong) NSString *clientId;
@property(nonatomic, assign) BOOL isInitiator;
@property(nonatomic, assign) BOOL isSpeakerEnabled;
@property(nonatomic, strong) NSMutableArray *iceServers;
@property(nonatomic, strong) RTCAudioTrack *defaultAudioTrack;
@property(nonatomic, strong) RTCVideoTrack *defaultVideoTrack;

@property(nonatomic, strong) NSString *channelId;
@property(nonatomic, strong) NSString *callUser;

@end

@implementation ARDAppClient

@synthesize delegate = _delegate;
@synthesize state = _state;
@synthesize serverHostUrl = _serverHostUrl;
@synthesize peerConnection = _peerConnection;
@synthesize factory = _factory;
@synthesize messageQueue = _messageQueue;
@synthesize isTurnComplete = _isTurnComplete;
@synthesize hasReceivedSdp  = _hasReceivedSdp;
@synthesize roomId = _roomId;
@synthesize clientId = _clientId;
@synthesize isInitiator = _isInitiator;
@synthesize isSpeakerEnabled = _isSpeakerEnabled;
@synthesize iceServers = _iceServers;

@synthesize channelId = _channelId;
@synthesize callUser = _callUser;
@synthesize pubNubClient = _pubNubClient;

- (instancetype)initWithDelegate:(id<ARDAppClientDelegate>)delegate {
  if (self = [super init]) {
    _delegate = delegate;
    _factory = [[RTCPeerConnectionFactory alloc] init];
    _messageQueue = [NSMutableArray array];
    _iceServers = [NSMutableArray arrayWithObject:[self defaultSTUNServer]];
    _isSpeakerEnabled = YES;
      
      [[NSNotificationCenter defaultCenter] addObserver:self
                                               selector:@selector(orientationChanged:)
                                                   name:@"UIDeviceOrientationDidChangeNotification"
                                                 object:nil];
  }
  return self;
}

- (void)dealloc {
  [[NSNotificationCenter defaultCenter] removeObserver:self name:@"UIDeviceOrientationDidChangeNotification" object:nil];
  [[NSNotificationCenter defaultCenter] removeObserver:self name:@"pubNubMessage" object:nil];
  [self disconnect];
}

- (void)orientationChanged:(NSNotification *)notification {
//    UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
//    if (UIDeviceOrientationIsLandscape(orientation) || UIDeviceOrientationIsPortrait(orientation)) {
        //Remove current video track
        RTCMediaStream *localStream = _peerConnection.localStreams[0];
        [localStream removeVideoTrack:localStream.videoTracks[0]];
        
        RTCVideoTrack *localVideoTrack = [self createLocalVideoTrack];
        if (localVideoTrack) {
            [localStream addVideoTrack:localVideoTrack];
            [_delegate appClient:self didReceiveLocalVideoTrack:localVideoTrack];
        }
        [_peerConnection removeStream:localStream];
        [_peerConnection addStream:localStream];
//    }
}

- (void)connectToChannelWithPubNub:(NSString *)channelId callUser:(NSString *)callUser pubNubClient:(PubNub*)pubNubClient{
    
    [self disconnect];
    _isInitiator = YES;
    _channelId = channelId;
    _callUser = callUser;
    _pubNubClient = pubNubClient;
    
    _iceServers = [self addICeServers];
    _isTurnComplete = YES;
    _hasReceivedSdp = YES;
    [self startSignalingIfReady];
    
}

- (void)connectToChannelWithId:(NSString *)channelId callUser:(NSString *)callUser {
    
    [self disconnect];
    _isInitiator = YES;
    _channelId = channelId;
    _callUser = callUser;
    
    _iceServers = [self addICeServers];
    _isTurnComplete = YES;
    _hasReceivedSdp = YES;
    [self startSignalingIfReady];

}

- (void)createOfferPeerConnectionAction:(NSString *)channelId callUser:(NSString *)callUser {
    
    RTCSessionDescription *description = [[RTCSessionDescription alloc] initWithType:@"answer" sdp:callUser];
    [_peerConnection setLocalDescriptionWithDelegate:self sessionDescription:description];
    [_peerConnection createOfferWithDelegate:self constraints:[self defaultOfferConstraints]];
    
    //[_peerConnection createAnswerWithDelegate:self constraints:[self defaultOfferConstraints]];
    
}

- (void)addCandidateIntoPeerConnection:(NSString *)callUser{
    
    RTCICECandidate *candidate = [[RTCICECandidate alloc] initWithMid:@"audio" index:0 sdp:callUser];
    [_peerConnection addICECandidate:candidate];
    
}

- (void)setPeerRemoteConnection:(NSString *)channelId callUser:(NSString *)callUser SessionDescription:(RTCSessionDescription*)SessionDescription{
    
    //RTCSessionDescription *description = [[RTCSessionDescription alloc] initWithType:@"answer" sdp:callUser];
    [_peerConnection setRemoteDescriptionWithDelegate:self sessionDescription:SessionDescription];
    
//    RTCSessionDescription *description = [[RTCSessionDescription alloc] initWithType:@"answer" sdp:SessionDescription.description];
//    [_peerConnection setRemoteDescriptionWithDelegate:self sessionDescription:description];
    
}

- (void)setPeerRemoteConnection:(NSString *)channelId callUser:(NSString *)callUser {
    
    //RTCSessionDescription *description = [[RTCSessionDescription alloc] initWithType:channelId sdp:callUser];
    //RTCSessionDescription *description = [[RTCSessionDescription alloc] initWithType:@"answer" sdp:callUser];
    RTCSessionDescription *description = [[RTCSessionDescription alloc] initWithType:@"answer" sdp:channelId];
    //RTCSessionDescription *description = [[RTCSessionDescription alloc] initWithType:@"offer" sdp:channelId];
    //RTCSessionDescription *description = [[RTCSessionDescription alloc] initWithType:@"offer" sdp:callUser];
    
    //[_peerConnection createOfferWithDelegate:self constraints:[self defaultOfferConstraints]];
    
    [_peerConnection setRemoteDescriptionWithDelegate:self sessionDescription:description];
    
}

- (NSMutableArray *)addICeServers{
    
    if (_iceServers == nil) {
        _iceServers = [[NSMutableArray alloc] init];
    }
        
//    [_iceServers addObject:[[RTCICEServer alloc] initWithURI:[NSURL URLWithString:@"stun:e5.xirsys.com"]
//                                                   username:@"603febb8-5e6e-11e8-8742-3a11f5d3b782"
//                                                   password:@"603fec26-5e6e-11e8-ab1a-a6a06791d4fd"]];
    [_iceServers addObject:[[RTCICEServer alloc] initWithURI:[NSURL URLWithString:@"turn:e5.xirsys.com:80?transport=tcp"]
                                                    username:@"603febb8-5e6e-11e8-8742-3a11f5d3b782"
                                                    password:@"603fec26-5e6e-11e8-ab1a-a6a06791d4fd"]];
    [_iceServers addObject:[[RTCICEServer alloc] initWithURI:[NSURL URLWithString:@"turn:e5.xirsys.com:80?transport=udp"]
                                                    username:@"603febb8-5e6e-11e8-8742-3a11f5d3b782"
                                                    password:@"603fec26-5e6e-11e8-ab1a-a6a06791d4fd"]];
    [_iceServers addObject:[[RTCICEServer alloc] initWithURI:[NSURL URLWithString:@"turn:e5.xirsys.com:3478?transport=udp"]
                                                    username:@"603febb8-5e6e-11e8-8742-3a11f5d3b782"
                                                    password:@"603fec26-5e6e-11e8-ab1a-a6a06791d4fd"]];
    [_iceServers addObject:[[RTCICEServer alloc] initWithURI:[NSURL URLWithString:@"turn:e5.xirsys.com:80?transport=tcp"]
                                                    username:@"603febb8-5e6e-11e8-8742-3a11f5d3b782"
                                                    password:@"603fec26-5e6e-11e8-ab1a-a6a06791d4fd"]];
    [_iceServers addObject:[[RTCICEServer alloc] initWithURI:[NSURL URLWithString:@"turn:e5.xirsys.com:3478?transport=tcp"]
                                                    username:@"603febb8-5e6e-11e8-8742-3a11f5d3b782"
                                                    password:@"603fec26-5e6e-11e8-ab1a-a6a06791d4fd"]];
    [_iceServers addObject:[[RTCICEServer alloc] initWithURI:[NSURL URLWithString:@"turns:e5.xirsys.com:443?transport=tcp"]
                                                    username:@"603febb8-5e6e-11e8-8742-3a11f5d3b782"
                                                    password:@"603fec26-5e6e-11e8-ab1a-a6a06791d4fd"]];
    [_iceServers addObject:[[RTCICEServer alloc] initWithURI:[NSURL URLWithString:@"turns:e5.xirsys.com:5349?transport=tcp"]
                                                    username:@"603febb8-5e6e-11e8-8742-3a11f5d3b782"
                                                    password:@"603fec26-5e6e-11e8-ab1a-a6a06791d4fd"]];
    
    return _iceServers;
}

- (void)disconnect {
//  if (_state == kARDAppClientStateDisconnected) {
//    return;
//  }
////  if (self.isRegisteredWithRoomServer) {
////    [self unregisterWithRoomServer];
////  }
//  if (_channel) {
//    if (_channel.state == kARDWebSocketChannelStateRegistered) {
//      // Tell the other client we're hanging up.
//      ARDByeMessage *byeMessage = [[ARDByeMessage alloc] init];
//      NSData *byeData = [byeMessage JSONData];
//      [_channel sendData:byeData];
//    }
//    // Disconnect from collider.
//    _channel = nil;
//  }
//  _clientId = nil;
//  _roomId = nil;
//  _isInitiator = NO;
//  _hasReceivedSdp = NO;
//  _messageQueue = [NSMutableArray array];
//  _peerConnection = nil;
//  self.state = kARDAppClientStateDisconnected;
}

#pragma mark - RTCPeerConnectionDelegate
- (void)peerConnection:(RTCPeerConnection *)peerConnection signalingStateChanged:(RTCSignalingState)stateChanged {
  NSLog(@"Signaling state changed: %d", stateChanged);
}

- (void)peerConnection:(RTCPeerConnection *)peerConnection addedStream:(RTCMediaStream *)stream {
  dispatch_async(dispatch_get_main_queue(), ^{
    NSLog(@"Received %lu video tracks and %lu audio tracks",
        (unsigned long)stream.videoTracks.count,
        (unsigned long)stream.audioTracks.count);
    if (stream.videoTracks.count) {
      RTCVideoTrack *videoTrack = stream.videoTracks[0];
        [self->_delegate appClient:self didReceiveRemoteVideoTrack:videoTrack];
        if (self->_isSpeakerEnabled) [self enableSpeaker]; //Use the "handsfree" speaker instead of the ear speaker.
    }
  });
}

- (void)peerConnection:(RTCPeerConnection *)peerConnection removedStream:(RTCMediaStream *)stream {
  NSLog(@"Stream was removed.");
}

- (void)peerConnectionOnRenegotiationNeeded:(RTCPeerConnection *)peerConnection {
  NSLog(@"WARNING: Renegotiation needed but unimplemented.");
    
    dispatch_async(dispatch_get_main_queue(), ^{
        for (RTCMediaStream *sStream in peerConnection.localStreams) {
            if (sStream.videoTracks.count) {
                RTCVideoTrack *videoTrack = sStream.videoTracks[0];
                
                NSLog(@"Received %lu video tracks", (unsigned long)sStream.videoTracks.count);
                
                [self->_delegate appClient:self didReceiveRemoteVideoTrack:videoTrack];
                if (self->_isSpeakerEnabled) [self enableSpeaker]; //Use the "handsfree" speaker instead of the ear speaker.
                
            }
        }
    });
    
}

- (void)peerConnection:(RTCPeerConnection *)peerConnection iceConnectionChanged:(RTCICEConnectionState)newState {
  NSLog(@"ICE state changed: %d", newState);
}

- (void)peerConnection:(RTCPeerConnection *)peerConnection iceGatheringChanged:(RTCICEGatheringState)newState {
  NSLog(@"ICE gathering state changed: %d", newState);
}

- (void)peerConnection:(RTCPeerConnection *)peerConnection gotICECandidate:(RTCICECandidate *)candidate {
  dispatch_async(dispatch_get_main_queue(), ^{
       NSLog(@"didOpenDataChannel");
  });
}

- (void)peerConnection:(RTCPeerConnection*)peerConnection didOpenDataChannel:(RTCDataChannel*)dataChannel {
    
     NSLog(@"didOpenDataChannel");
}

#pragma mark - RTCSessionDescriptionDelegate

- (void)peerConnection:(RTCPeerConnection *)peerConnection didCreateSessionDescription:(RTCSessionDescription *)sdp
                          error:(NSError *)error {
  dispatch_async(dispatch_get_main_queue(), ^{
    if (error) {
      NSLog(@"Failed to create session description. Error: %@", error);
      [self disconnect];
      NSDictionary *userInfo = @{
        NSLocalizedDescriptionKey: @"Failed to create session description.",
      };
      NSError *sdpError = [[NSError alloc] initWithDomain:kARDAppClientErrorDomain
                                     code:kARDAppClientErrorCreateSDP
                                 userInfo:userInfo];
        [self->_delegate appClient:self didError:sdpError];
      return;
    }
      
      [self setPeerRemoteConnection:self->_channelId callUser:self->_callUser SessionDescription:sdp];
      
  });
}

- (void)peerConnection:(RTCPeerConnection *)peerConnection didSetSessionDescriptionWithError:(NSError *)error {
  dispatch_async(dispatch_get_main_queue(), ^{
    if (error) {
      NSLog(@"Failed to set session description. Error: %@", error);
      [self disconnect];
      NSDictionary *userInfo = @{
        NSLocalizedDescriptionKey: @"Failed to set session description.",
      };
      NSError *sdpError = [[NSError alloc] initWithDomain:kARDAppClientErrorDomain
                                     code:kARDAppClientErrorSetSDP
                                 userInfo:userInfo];
        [self->_delegate appClient:self didError:sdpError];
      return;
    }
    // If we're answering and we've just set the remote offer we need to create
    // an answer and set the local description.
//    if (!self->_isInitiator && !self->_peerConnection.localDescription) {
      RTCMediaConstraints *constraints = [self defaultAnswerConstraints];
      [self->_peerConnection createAnswerWithDelegate:self constraints:constraints];

//    }
      
//      NSLog(@"Failed to set session description. Error: %@", error);
//      RTCMediaConstraints *constraints = [self defaultAnswerConstraints];
//      [self->_peerConnection createAnswerWithDelegate:self constraints:constraints];
  });
}

#pragma mark - Private

- (BOOL)isRegisteredWithRoomServer {
  return _clientId.length;
}

- (void)startSignalingIfReady {
    
  // Create peer connection.
  RTCMediaConstraints *constraints = [self defaultPeerConnectionConstraints];
  _peerConnection = [_factory peerConnectionWithICEServers:_iceServers
                                               constraints:constraints
                                                  delegate:self];
  RTCMediaStream *localStream = [self createLocalMediaStream];
  [_peerConnection addStream:localStream];
  if (_isInitiator) {
    [self sendOffer];
  } else {
    [self waitForAnswer];
  }
}

- (void)sendOffer {
    
    [self createOfferPeerConnectionAction:_channelId callUser:_callUser];
}

- (void)waitForAnswer {
    
    [self setPeerRemoteConnection:_channelId callUser:_callUser];
}

- (void)processSignalingMessage:(ARDSignalingMessage *)message {
  NSParameterAssert(_peerConnection ||
      message.type == kARDSignalingMessageTypeBye);
  switch (message.type) {
    case kARDSignalingMessageTypeOffer:
    case kARDSignalingMessageTypeAnswer: {
      ARDSessionDescriptionMessage *sdpMessage =
          (ARDSessionDescriptionMessage *)message;
      RTCSessionDescription *description = sdpMessage.sessionDescription;
      [_peerConnection setRemoteDescriptionWithDelegate:self
                                     sessionDescription:description];
      break;
    }
    case kARDSignalingMessageTypeCandidate: {
      ARDICECandidateMessage *candidateMessage =
          (ARDICECandidateMessage *)message;
      [_peerConnection addICECandidate:candidateMessage.candidate];
      break;
    }
    case kARDSignalingMessageTypeBye:
      // Other client disconnected.
      // TODO(tkchin): support waiting in room for next client. For now just
      // disconnect.
      [self disconnect];
      break;
  }
}

- (RTCVideoTrack *)createLocalVideoTrack {
    // The iOS simulator doesn't provide any sort of camera capture
    // support or emulation (http://goo.gl/rHAnC1) so don't bother
    // trying to open a local stream.
    // TODO(tkchin): local video capture for OSX. See
    // https://code.google.com/p/webrtc/issues/detail?id=3417.

    RTCVideoTrack *localVideoTrack = nil;
#if !TARGET_IPHONE_SIMULATOR && TARGET_OS_IPHONE

    AVCaptureDeviceDiscoverySession *captureDeviceDiscoverySession = [AVCaptureDeviceDiscoverySession discoverySessionWithDeviceTypes:@[AVCaptureDeviceTypeBuiltInWideAngleCamera]
                                                                                                                            mediaType:AVMediaTypeVideo
                                                                                                                             position:AVCaptureDevicePositionFront];
    NSArray *captureDevices = [captureDeviceDiscoverySession devices];
    NSString *cameraID = nil;
    for (AVCaptureDevice *captureDevice in captureDevices) {
        if (captureDevice.position == AVCaptureDevicePositionFront) {
            cameraID = [captureDevice localizedName];
            break;
        }
    }
    
//    NSString *cameraID = nil;
//    for (AVCaptureDevice *captureDevice in
//         [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo]) {
//        if (captureDevice.position == AVCaptureDevicePositionFront) {
//            cameraID = [captureDevice localizedName];
//            break;
//        }
//    }
    
    NSAssert(cameraID, @"Unable to get the front camera id");
    
    RTCVideoCapturer *capturer = [RTCVideoCapturer capturerWithDeviceName:cameraID];
    RTCMediaConstraints *mediaConstraints = [self defaultMediaStreamConstraints];
    RTCVideoSource *videoSource = [_factory videoSourceWithCapturer:capturer constraints:mediaConstraints];
     localVideoTrack = [_factory videoTrackWithID:@"videoPN" source:videoSource];
#endif
    return localVideoTrack;
}

- (RTCMediaStream *)createLocalMediaStream {
    RTCMediaStream* localStream = [_factory mediaStreamWithLabel:@"localStreamPN"];

    RTCVideoTrack *localVideoTrack = [self createLocalVideoTrack];
    if (localVideoTrack) {
        [localStream addVideoTrack:localVideoTrack];
        [_delegate appClient:self didReceiveLocalVideoTrack:localVideoTrack];
    }
    
     [localStream addAudioTrack:[_factory audioTrackWithID:@"audioPN"]];
    
    if (_isSpeakerEnabled) [self enableSpeaker];
    return localStream;
}

#pragma mark - Defaults
- (RTCMediaConstraints *)defaultMediaStreamConstraints {
  RTCMediaConstraints* constraints =
      [[RTCMediaConstraints alloc]
          initWithMandatoryConstraints:nil
                   optionalConstraints:nil];
  return constraints;
}

- (RTCMediaConstraints *)defaultAnswerConstraints {
  return [self defaultOfferConstraints];
}

- (RTCMediaConstraints *)defaultOfferConstraints {
  NSArray *mandatoryConstraints = @[
      [[RTCPair alloc] initWithKey:@"OfferToReceiveAudio" value:@"true"],
      [[RTCPair alloc] initWithKey:@"OfferToReceiveVideo" value:@"true"]
  ];
  RTCMediaConstraints* constraints =
      [[RTCMediaConstraints alloc]
          initWithMandatoryConstraints:mandatoryConstraints
                   optionalConstraints:nil];
  return constraints;
}

- (RTCMediaConstraints *)defaultPeerConnectionConstraints {
    
NSArray *mandatoryConstraints = @[
                                    [[RTCPair alloc] initWithKey:@"OfferToReceiveAudio" value:@"true"],
                                    [[RTCPair alloc] initWithKey:@"OfferToReceiveVideo" value:@"true"]
                                    ];
  NSArray *optionalConstraints = @[
      [[RTCPair alloc] initWithKey:@"DtlsSrtpKeyAgreement" value:@"true"]
  ];
  RTCMediaConstraints* constraints =
      [[RTCMediaConstraints alloc]
          initWithMandatoryConstraints:mandatoryConstraints
                   optionalConstraints:optionalConstraints];
  return constraints;
}

- (RTCICEServer *)defaultSTUNServer {
  NSURL *defaultSTUNServerURL = [NSURL URLWithString:kDefaultSTUNServerUrl];
  return [[RTCICEServer alloc] initWithURI:defaultSTUNServerURL
                                  username:kDefaultSTUNServerUsername
                                  password:kDefaultSTUNServerPassword];
}

#pragma mark - Audio mute/unmute
- (void)muteAudioIn {
    NSLog(@"audio muted");
    RTCMediaStream *localStream = _peerConnection.localStreams[0];
    self.defaultAudioTrack = localStream.audioTracks[0];
    [localStream removeAudioTrack:localStream.audioTracks[0]];
    [_peerConnection removeStream:localStream];
    [_peerConnection addStream:localStream];
}
- (void)unmuteAudioIn {
    NSLog(@"audio unmuted");
    RTCMediaStream* localStream = _peerConnection.localStreams[0];
    [localStream addAudioTrack:self.defaultAudioTrack];
    [_peerConnection removeStream:localStream];
    [_peerConnection addStream:localStream];
    if (_isSpeakerEnabled) [self enableSpeaker];
}

#pragma mark - Video mute/unmute
- (void)muteVideoIn {
    NSLog(@"video muted");
    RTCMediaStream *localStream = _peerConnection.localStreams[0];
    self.defaultVideoTrack = localStream.videoTracks[0];
    [localStream removeVideoTrack:localStream.videoTracks[0]];
    [_peerConnection removeStream:localStream];
    [_peerConnection addStream:localStream];
}
- (void)unmuteVideoIn {
    NSLog(@"video unmuted");
    RTCMediaStream* localStream = _peerConnection.localStreams[0];
    [localStream addVideoTrack:self.defaultVideoTrack];
    [_peerConnection removeStream:localStream];
    [_peerConnection addStream:localStream];
}

#pragma mark - swap camera
- (RTCVideoTrack *)createLocalVideoTrackBackCamera {
    RTCVideoTrack *localVideoTrack = nil;
#if !TARGET_IPHONE_SIMULATOR && TARGET_OS_IPHONE
    
    AVCaptureDeviceDiscoverySession *captureDeviceDiscoverySession = [AVCaptureDeviceDiscoverySession discoverySessionWithDeviceTypes:@[AVCaptureDeviceTypeBuiltInWideAngleCamera]
                                                                                                                            mediaType:AVMediaTypeVideo
                                                                                                                             position:AVCaptureDevicePositionFront];
    NSArray *captureDevices = [captureDeviceDiscoverySession devices];
    NSString *cameraID = nil;
    for (AVCaptureDevice *captureDevice in captureDevices) {
        if (captureDevice.position == AVCaptureDevicePositionFront) {
            cameraID = [captureDevice localizedName];
            break;
        }
    }
    
//    //AVCaptureDevicePositionFront
//    NSString *cameraID = nil;
//    for (AVCaptureDevice *captureDevice in
//         [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo]) {
//        if (captureDevice.position == AVCaptureDevicePositionBack) {
//            cameraID = [captureDevice localizedName];
//            break;
//        }
//    }
    NSAssert(cameraID, @"Unable to get the back camera id");
    
    RTCVideoCapturer *capturer = [RTCVideoCapturer capturerWithDeviceName:cameraID];
    RTCMediaConstraints *mediaConstraints = [self defaultMediaStreamConstraints];
    RTCVideoSource *videoSource = [_factory videoSourceWithCapturer:capturer constraints:mediaConstraints];
    localVideoTrack = [_factory videoTrackWithID:@"videoPN" source:videoSource];
    
#endif
    return localVideoTrack;
}
- (void)swapCameraToFront{
    RTCMediaStream *localStream = _peerConnection.localStreams[0];
    [localStream removeVideoTrack:localStream.videoTracks[0]];
    
    RTCVideoTrack *localVideoTrack = [self createLocalVideoTrack];

    if (localVideoTrack) {
        [localStream addVideoTrack:localVideoTrack];
        [_delegate appClient:self didReceiveLocalVideoTrack:localVideoTrack];
    }
    [_peerConnection removeStream:localStream];
    [_peerConnection addStream:localStream];
}
- (void)swapCameraToBack{
    RTCMediaStream *localStream = _peerConnection.localStreams[0];
    [localStream removeVideoTrack:localStream.videoTracks[0]];
    
    RTCVideoTrack *localVideoTrack = [self createLocalVideoTrackBackCamera];
    
    if (localVideoTrack) {
        [localStream addVideoTrack:localVideoTrack];
        [_delegate appClient:self didReceiveLocalVideoTrack:localVideoTrack];
    }
    [_peerConnection removeStream:localStream];
    [_peerConnection addStream:localStream];
}

#pragma mark - enable/disable speaker

- (void)enableSpeaker {
    [[AVAudioSession sharedInstance] overrideOutputAudioPort:AVAudioSessionPortOverrideSpeaker error:nil];
    _isSpeakerEnabled = YES;
}

- (void)disableSpeaker {
    [[AVAudioSession sharedInstance] overrideOutputAudioPort:AVAudioSessionPortOverrideNone error:nil];
    _isSpeakerEnabled = NO;
}

@end
